using ons.exemplogerador.entidade;
using ons.exemplogerador.servico.ioc.webapi;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.OData.Batch;
using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using System.Web.Http.OData.Routing.Conventions;

namespace ons.exemplogerador.servico
{
	/// <summary>
	/// WebApiConfig de acesso a base ExemploGerador2
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:06
    /// </remarks>
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var builder = new ODataConventionModelBuilder();
			
			//Animal
			var entityTypeAnimal = builder.EntitySet<Animal>("Animal").EntityType;
			entityTypeAnimal.HasKey(t => t.IdAnimal);
			//Exemplo de método customizado.
			//var actionConfigAnimal = entityTypeAnimal.Collection.Action("ObterAnimal");
            //actionConfigAnimal.ReturnsCollectionFromEntitySet<Animal>("Animal");
            //actionConfigAnimal.Parameter<int>("[Filtro]");
			
			//Pessoa
			var entityTypePessoa = builder.EntitySet<Pessoa>("Pessoa").EntityType;
			entityTypePessoa.HasKey(t => t.IdPessoa);
			//Exemplo de método customizado.
			//var actionConfigPessoa = entityTypePessoa.Collection.Action("ObterPessoa");
            //actionConfigPessoa.ReturnsCollectionFromEntitySet<Pessoa>("Pessoa");
            //actionConfigPessoa.Parameter<int>("[Filtro]");
			
			//Pessoaanimal
			var entityTypePessoaanimal = builder.EntitySet<Pessoaanimal>("Pessoaanimal").EntityType;
			entityTypePessoaanimal.HasKey(t => t.IdPessoaanimal);
			//Exemplo de método customizado.
			//var actionConfigPessoaanimal = entityTypePessoaanimal.Collection.Action("ObterPessoaanimal");
            //actionConfigPessoaanimal.ReturnsCollectionFromEntitySet<Pessoaanimal>("Pessoaanimal");
            //actionConfigPessoaanimal.Parameter<int>("[Filtro]");
			
			//Statuspessoa
			var entityTypeStatuspessoa = builder.EntitySet<Statuspessoa>("Statuspessoa").EntityType;
			entityTypeStatuspessoa.HasKey(t => t.IdStatuspessoa);
			//Exemplo de método customizado.
			//var actionConfigStatuspessoa = entityTypeStatuspessoa.Collection.Action("ObterStatuspessoa");
            //actionConfigStatuspessoa.ReturnsCollectionFromEntitySet<Statuspessoa>("Statuspessoa");
            //actionConfigStatuspessoa.Parameter<int>("[Filtro]");
			
			//Tipoanimal
			var entityTypeTipoanimal = builder.EntitySet<Tipoanimal>("Tipoanimal").EntityType;
			entityTypeTipoanimal.HasKey(t => t.IdTipoanimal);
			//Exemplo de método customizado.
			//var actionConfigTipoanimal = entityTypeTipoanimal.Collection.Action("ObterTipoanimal");
            //actionConfigTipoanimal.ReturnsCollectionFromEntitySet<Tipoanimal>("Tipoanimal");
            //actionConfigTipoanimal.Parameter<int>("[Filtro]");
			


            var batchHandler = new DefaultODataBatchHandler(GlobalConfiguration.DefaultServer);
            batchHandler.MessageQuotas.MaxOperationsPerChangeset = 1500;

            IList<IODataRoutingConvention> routingConventions = ODataRoutingConventions.CreateDefault();
            routingConventions.Insert(0, new CountODataRoutingConvention());
			routingConventions.Insert(0, new CompositeKeyRoutingConvention());

             config.Routes.MapODataServiceRoute("odata", "odata/v1", builder.GetEdmModel(), new CountODataPathHandler(), routingConventions, batchHandler);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "api",
                routeTemplate: "api/v1/{controller}/{action}",
                defaults: new { id = RouteParameter.Optional }
            );

        }
    }
}


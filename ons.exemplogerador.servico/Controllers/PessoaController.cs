using ons.exemplogerador.entidade;
using ons.exemplogerador.negocio.contrato;
using ons.exemplogerador.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.exemplogerador.servico
{

	/// <summary>
	/// Controller Wep Api para: Tabela com as informações das pessoas donas de animais
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	public partial class PessoaController : BaseController<Pessoa>
	{
        private IPessoaNegocio PessoaNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public PessoaController(IPessoaNegocio pessoaNegocio) : base(pessoaNegocio)
		{
			PessoaNegocio = pessoaNegocio;
		}
		
		// GET: odata/Pessoa
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<Pessoa> Get()
        {
            return Query();
        }

        // GET: odata/Pessoa
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<Pessoa> Get([FromODataUri] int key)
        {
            return Carregar(item => item.IdPessoa == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] int key, string navigationProperty, string type)
        {
            return GetProperty(item => item.IdPessoa == key, navigationProperty, type);
        }

		// PUT: odata/Pessoa
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] int key, Delta<Pessoa> delta)
        {
			return base.Put(item => item.IdPessoa == key, delta);
        }

        // POST: odata/Pessoa
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(Pessoa pessoa)
        {
			return base.Post(pessoa);
        }

        // PATCH: odata/Pessoa
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Pessoa> delta)
        {
			return base.Patch(item => item.IdPessoa == key, delta);
        }

        // DELETE: odata/Pessoa
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] int key)
        {
			return base.Delete(item => item.IdPessoa == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            PessoaNegocio = null;
            base.Dispose(disposing);
        }
	}
}

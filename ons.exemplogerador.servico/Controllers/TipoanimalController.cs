using ons.exemplogerador.entidade;
using ons.exemplogerador.negocio.contrato;
using ons.exemplogerador.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.exemplogerador.servico
{

	/// <summary>
	/// Controller Wep Api para: Tabela de tipo de Animal. Ex.: Cachorro, Gato, Tartaruga, Cobra, etc.
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:14
    /// </remarks>
	public partial class TipoanimalController : BaseController<Tipoanimal>
	{
        private ITipoanimalNegocio TipoanimalNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public TipoanimalController(ITipoanimalNegocio tipoanimalNegocio) : base(tipoanimalNegocio)
		{
			TipoanimalNegocio = tipoanimalNegocio;
		}
		
		// GET: odata/Tipoanimal
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<Tipoanimal> Get()
        {
            return Query();
        }

        // GET: odata/Tipoanimal
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<Tipoanimal> Get([FromODataUri] int key)
        {
            return Carregar(item => item.IdTipoanimal == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] int key, string navigationProperty, string type)
        {
            return GetProperty(item => item.IdTipoanimal == key, navigationProperty, type);
        }

		// PUT: odata/Tipoanimal
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] int key, Delta<Tipoanimal> delta)
        {
			return base.Put(item => item.IdTipoanimal == key, delta);
        }

        // POST: odata/Tipoanimal
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(Tipoanimal tipoanimal)
        {
			return base.Post(tipoanimal);
        }

        // PATCH: odata/Tipoanimal
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Tipoanimal> delta)
        {
			return base.Patch(item => item.IdTipoanimal == key, delta);
        }

        // DELETE: odata/Tipoanimal
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] int key)
        {
			return base.Delete(item => item.IdTipoanimal == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            TipoanimalNegocio = null;
            base.Dispose(disposing);
        }
	}
}

using ons.exemplogerador.entidade;
using ons.exemplogerador.negocio.contrato;
using ons.exemplogerador.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.exemplogerador.servico
{

	/// <summary>
	/// Controller Wep Api para: Tabela com as informações dos animais
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	public partial class AnimalController : BaseController<Animal>
	{
        private IAnimalNegocio AnimalNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public AnimalController(IAnimalNegocio animalNegocio) : base(animalNegocio)
		{
			AnimalNegocio = animalNegocio;
		}
		
		// GET: odata/Animal
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<Animal> Get()
        {
            return Query();
        }

        // GET: odata/Animal
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<Animal> Get([FromODataUri] int key)
        {
            return Carregar(item => item.IdAnimal == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] int key, string navigationProperty, string type)
        {
            return GetProperty(item => item.IdAnimal == key, navigationProperty, type);
        }

		// PUT: odata/Animal
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] int key, Delta<Animal> delta)
        {
			return base.Put(item => item.IdAnimal == key, delta);
        }

        // POST: odata/Animal
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(Animal animal)
        {
			return base.Post(animal);
        }

        // PATCH: odata/Animal
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Animal> delta)
        {
			return base.Patch(item => item.IdAnimal == key, delta);
        }

        // DELETE: odata/Animal
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] int key)
        {
			return base.Delete(item => item.IdAnimal == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            AnimalNegocio = null;
            base.Dispose(disposing);
        }
	}
}

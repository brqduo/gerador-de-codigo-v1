﻿using ons.exemplogerador.entidade;
using ons.exemplogerador.negocio.contrato;
using ons.exemplogerador.servico.ioc.webapi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ons.exemplogerador.servico.Controllers
{
    [LogActionFilter]
    [MessageHandleError]
    public class RelatorioController : ApiController
    {
        ITipoanimalNegocio TipoanimalNegocio;

        public RelatorioController(ITipoanimalNegocio tipoanimalNegocio)
        {
            TipoanimalNegocio = tipoanimalNegocio;
        }


        [POPAuthorize("Consultar Tipoanimal")]
        [HttpPost]
        public List<RelAnimal> ObterQuantidadeAnimalPorTipo()
        {
            return TipoanimalNegocio.ObterQuantidadeAnimalPorTipo();
        }
    }
}

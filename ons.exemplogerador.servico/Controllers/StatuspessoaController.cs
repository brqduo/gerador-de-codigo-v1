using ons.exemplogerador.entidade;
using ons.exemplogerador.negocio.contrato;
using ons.exemplogerador.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.exemplogerador.servico
{

	/// <summary>
	/// Controller Wep Api para: Tabela com o status da pessoa. Ex.: Dono, Interessado em adotar ou Em processo de adoção
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:14
    /// </remarks>
	public partial class StatuspessoaController : BaseController<Statuspessoa>
	{
        private IStatuspessoaNegocio StatuspessoaNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public StatuspessoaController(IStatuspessoaNegocio statuspessoaNegocio) : base(statuspessoaNegocio)
		{
			StatuspessoaNegocio = statuspessoaNegocio;
		}
		
		// GET: odata/Statuspessoa
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<Statuspessoa> Get()
        {
            return Query();
        }

        // GET: odata/Statuspessoa
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<Statuspessoa> Get([FromODataUri] int key)
        {
            return Carregar(item => item.IdStatuspessoa == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] int key, string navigationProperty, string type)
        {
            return GetProperty(item => item.IdStatuspessoa == key, navigationProperty, type);
        }

		// PUT: odata/Statuspessoa
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] int key, Delta<Statuspessoa> delta)
        {
			return base.Put(item => item.IdStatuspessoa == key, delta);
        }

        // POST: odata/Statuspessoa
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(Statuspessoa statuspessoa)
        {
			return base.Post(statuspessoa);
        }

        // PATCH: odata/Statuspessoa
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Statuspessoa> delta)
        {
			return base.Patch(item => item.IdStatuspessoa == key, delta);
        }

        // DELETE: odata/Statuspessoa
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] int key)
        {
			return base.Delete(item => item.IdStatuspessoa == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            StatuspessoaNegocio = null;
            base.Dispose(disposing);
        }
	}
}

using ons.exemplogerador.entidade;
using ons.exemplogerador.negocio.contrato;
using ons.exemplogerador.servico.ioc.webapi;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Query;

namespace ons.exemplogerador.servico
{

	/// <summary>
	/// Controller Wep Api para: Tabela Associativa Pessoa x Animal
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	public partial class PessoaanimalController : BaseController<Pessoaanimal>
	{
        private IPessoaanimalNegocio PessoaanimalNegocio;
        private static ODataValidationSettings _validationSettings = new ODataValidationSettings();

        public PessoaanimalController(IPessoaanimalNegocio pessoaanimalNegocio) : base(pessoaanimalNegocio)
		{
			PessoaanimalNegocio = pessoaanimalNegocio;
		}
		
		// GET: odata/Pessoaanimal
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public IQueryable<Pessoaanimal> Get()
        {
            return Query();
        }

        // GET: odata/Pessoaanimal
        [POPCRUDAPIAuthorize]
        [EnableQuery]
        public SingleResult<Pessoaanimal> Get([FromODataUri] int key)
        {
            return Carregar(item => item.IdPessoaanimal == key);
        }
		
        [POPCRUDAPIAuthorize]
        public HttpResponseMessage GetProperty([FromODataUri] int key, string navigationProperty, string type)
        {
            return GetProperty(item => item.IdPessoaanimal == key, navigationProperty, type);
        }

		// PUT: odata/Pessoaanimal
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Put([FromODataUri] int key, Delta<Pessoaanimal> delta)
        {
			return base.Put(item => item.IdPessoaanimal == key, delta);
        }

        // POST: odata/Pessoaanimal
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Post(Pessoaanimal pessoaanimal)
        {
			return base.Post(pessoaanimal);
        }

        // PATCH: odata/Pessoaanimal
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public IHttpActionResult Patch([FromODataUri] int key, Delta<Pessoaanimal> delta)
        {
			return base.Patch(item => item.IdPessoaanimal == key, delta);
        }

        // DELETE: odata/Pessoaanimal
        [POPCRUDAPIAuthorize]
        public IHttpActionResult Delete([FromODataUri] int key)
        {
			return base.Delete(item => item.IdPessoaanimal == key);
        }
		
		protected override void Dispose(bool disposing)
        {
            PessoaanimalNegocio = null;
            base.Dispose(disposing);
        }
	}
}

using System;
using System.Collections.Generic;

namespace ons.exemplogerador.contexto.contrato
{

	/// <summary>
	/// interface do DbContext de acesso a dados do Exemplogerador2
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	public interface IExemplogerador2DbContext : IDbContext
    {
        
    }
}



﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace ons.exemplogerador.servico.ioc.webapi
{
    [LogActionFilter]
    [MessageHandleError]
    public class BaseApiController : ApiController
    {
        public dynamic GetAppConfig()
        {
            var ExibeLog = (System.Configuration.ConfigurationManager.AppSettings["ExibeLog"] == "1");
            var LoginUrl = System.Web.Security.FormsAuthentication.LoginUrl;
            return new { ExibeLog, LoginUrl };
        }
    }
}

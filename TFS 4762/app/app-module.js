(function () {
  'use strict';
  /* @ngdoc object
   * @name gerador
   * @description
   *
   */
  angular
    .module('gerador', [
      'ngMaterial',
      'ngRoute',
      'agGrid',
      'home',
      'animal'
    ]);
}());
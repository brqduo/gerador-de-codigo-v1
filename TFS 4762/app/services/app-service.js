(function() {
    'use strict';
    
   angular
          .module('animal') // Define a qual módulo seu .service pertence
          .service('services', MeuService); //Define o nome a função do seu .service
    
          MeuService.$inject = ['$http']; //Lista de dependências
    
          function MeuService($http) {
    
            return {
              animal: function() {
                  return $http.get("http://10.2.1.127:3000/animal").then(function(response) {
                      return response.data;
                  });
              },
              pessoas: function() {
                return $http.get("http://10.2.1.127:3000/pessoas").then(function(response) {
                    return response.data;
                });
              },
              pessoasAngular: function() {
                return $http.get("http://10.2.1.127:3000/pessoasAngular").then(function(response) {
                    return response.data;
                });
              },
              tipoanimal: function() {
                return $http.get("http://10.2.1.127:3000/tipoanimal").then(function(response) {
                    return response.data;
                });
              },
              raca: function() {
                return $http.get("http://10.2.1.127:3000/raca").then(function(response) {
                    return response.data;
                });
              }
            }
              
          }
   })();
   
(function () {
    'use strict';
  
    angular
      .module('animal')
      .config(config);
  
    function config($routeProvider) {
      $routeProvider
        .when('/animal', {
          templateUrl: 'Animal/views/Animal.html',
          controller: 'AnimalController',
          controllerAs: 'animal'
        });
    }
  }());
  
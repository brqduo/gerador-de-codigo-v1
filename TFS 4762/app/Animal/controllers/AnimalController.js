(function () {
    'use strict';
    agGrid.initialiseAgGridWithAngular1(angular);
    /**
     * @ngdoc object
     * @name animal.controller:AnimalController
     *
     * @description
     *
     */
    angular
        .module('animal')
        .controller("AnimalController", function ($scope, $rootScope, $http, services) {
            $scope.animalObj = {
                animal: null,
                tipoAnimal: null,
                raca: null
            };

            $scope.gridOptions = {
                enableSorting: true,
                /* angularCompileRows: true, */
                enableFilter: true,
                rowData: null,
                columnDefs: null,
                rowSelection: 'single',
                onGridReady: function (event) {
                    event.api.sizeColumnsToFit();
                }
            };
            $scope.coluna = function () {
                var columnDefs = [{
                        headerName: "Tipo animal",
                        field: "Tipoanimal",
                        width: 500,
                    },
                    {
                        headerName: "Nome",
                        field: "NomeAnimal",
                        width: 700
                    },
                ];
                return columnDefs;
            }
            $scope.gridOptions.columnDefs = $scope.coluna();

            services.animal().then(function (data) {
                $scope.animalObj.animal = data;
                console.log(data);
                console.log('12344567');

                $scope.gridOptions.api.setColumnDefs($scope.coluna())
                $scope.gridOptions.api.setRowData(data)
            });
            services.tipoanimal().then(function (data) {
                $scope.animalObj.tipoAnimal = data;
                console.log(data);
            });
            services.raca().then(function (data) {
                $scope.animalObj.raca = data;
                console.log(data);
            });


            $scope.teste = function () {
                console.log('Eu existo!', $scope.animalObj)
                $scope.gridOptions.api.setRowData(JSON.parse($scope.animalObj.animal))

            }
            // Util.getListasEdicao($scope, function (resultado) { $scope.animal.listasEdicao = resultado; ObterItem(); });

            // function ObterItem()
            // {
            //     var filtro = App.animal.filtroSelectedItem();

            //     if (filtro) {
            //         Util.getItem(App.animal.filtroSelectedItem(), function (item) { $scope.animal.itemEdicao = item; });
            //     }
            //     else
            //     {
            //         $scope.animal.itemEdicao = {};
            //     }
            // }

            // $scope.Salvar = function () {
            //     var filtro = App.animal.filtroSelectedItem();

            //     if (filtro) {
            //         Util.Atualizar(filtro, $scope.animal.itemEdicao, ValidarRetornoSalvar);
            //     }
            //     else {
            //         $scope.animal.itemEdicao.IdAnimal = 0;

            //         Util.Inserir($scope.animal.itemEdicao, ValidarRetornoSalvar);
            //     }
            // };

            // function ValidarRetornoSalvar(retorno)
            // {
            //     Util.Alerta(retorno.Mensagem, retorno.Sucesso);

            //     if (retorno.Sucesso === true) {
            //         App.animal.FecharEdicao();
            //     }
            // }
        });

}());
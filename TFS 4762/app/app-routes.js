(function () {
  'use strict';

  angular
    .module('gerador')
    .config(config);

  function config($routeProvider) {
    $routeProvider
    .when('/animal', {
      templateUrl: 'Animal/views/Animal.html',
      controller: 'AnimalController',
      controllerAs: 'animal'
    })
    .otherwise({
      redirectTo: '/home'
    });
  }
}());

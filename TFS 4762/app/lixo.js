/* agGrid.initialiseAgGridWithAngular1(angular);
moment.locale('pt-BR');​
//Configuracoes geraes.
var App = angular.module("App", ['agGrid', 'ngMessages', 'ngAnimate', 'ngSanitize', 'ngMaterial']);​
App.globalConfig = {};​
App.log = function (msgLog) {
  if (App.globalConfig.ExibeLog) {
    console.log(msgLog);
  }
}​
App.autocomplete = {};​
App.autocomplete.keyPress = function ($event, objSearch, funcSelectedItem) {
  console.log($event.keyCode);
  if ($event.keyCode == 38) {
    objSearch.index = objSearch.index - 1;
    if (objSearch.index < 0)
      objSearch.index = 0;
  } else if ($event.keyCode == 40) {
    if (!objSearch.itens || objSearch.itens.length == 0) {
      if (objSearch.value == undefined | objSearch.value == '' | !objSearch.pesquisar)
        objSearch.itens = objSearch.options;
      else if (objSearch.pesquisar)
        objSearch.pesquisar(objSearch.value, null);
    } else {
      objSearch.index = objSearch.index + 1;
      if (objSearch.index > objSearch.itens.length - 1)
        objSearch.index = objSearch.itens.length - 1;
    }
  } else if ($event.keyCode == 13) {
    $event.stopPropagation();
    $event.preventDefault();
    //objSearch.carregando = true;
    if (objSearch.itens.length > objSearch.index && objSearch.index > -1 && funcSelectedItem)
      funcSelectedItem(objSearch.itens[objSearch.index]);
    //objSearch.carregando = false;
  }
}​
App.removeAccents = function (value) {
  return value
    .replace(/á/g, 'a')
    .replace(/â/g, 'a')
    .replace(/é/g, 'e')
    .replace(/è/g, 'e')
    .replace(/ê/g, 'e')
    .replace(/í/g, 'i')
    .replace(/ï/g, 'i')
    .replace(/ì/g, 'i')
    .replace(/ó/g, 'o')
    .replace(/ô/g, 'o')
    .replace(/ú/g, 'u')
    .replace(/ü/g, 'u')
    .replace(/ç/g, 'c')
    .replace(/ß/g, 's');
}​
App.containsIgnoringAccents = function (actual, expected) {
  if (angular.isObject(actual)) return false;
  actual = App.removeAccents(angular.lowercase('' + actual));
  expected = App.removeAccents(angular.lowercase('' + expected));​
  return actual.indexOf(expected) !== -1;
}​
// ex: 29/08/2004     obtem a data convertida em 20040829 
// ou 08/2004       obtem a data convertida em 20040800 
// ou 2004-08-29T10:20:30 obtem a data convertida em 20040829.102030
App.monthToComparableNumber = function (date) {
  //App.log(date);
  //if (date != undefined && date.toLocaleDateString != undefined && typeof date.toLocaleDateString === 'function') {
  //  date = date.toLocaleDateString();
  //  App.log(date);
  //}
  ​
  if (date === undefined || date === null || (date.length !== 10 && date.length !== 7 && date.length !== 19 && !typeof date.toLocaleDateString === 'function')) {
    return date;
  }​
  if (date.length == 10) {
    var yearNumber = date.substring(6, 10);
    var monthNumber = date.substring(3, 5);
    var dayNumber = date.substring(0, 2);
    var hourNumber = 0;
    var minNumber = 0;
    var segNumber = 0;
  } else if (date.length == 7) {
    var yearNumber = date.substring(3, 7);
    var monthNumber = date.substring(0, 2);
    var dayNumber = 0;
    var hourNumber = 0;
    var minNumber = 0;
    var segNumber = 0;
  } else if (date.length == 19) {
    var yearNumber = date.substring(0, 4);
    var monthNumber = date.substring(5, 7);
    var dayNumber = date.substring(8, 10);
    var hourNumber = date.substring(11, 13);
    var minNumber = date.substring(14, 16);
    var segNumber = date.substring(17, 19);
  } else if (typeof date.toLocaleDateString === 'function') {
    var yearNumber = date.getFullYear();
    var monthNumber = date.getMonth() + 1;
    var dayNumber = date.getDate();
    var hourNumber = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
    var minNumber = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
    var segNumber = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();;
  }​
  var result = (yearNumber * 10000) + (monthNumber * 100) + (dayNumber * 1) + (hourNumber * 0.01) + (minNumber * 0.0001) + (segNumber * 0.000001);
  //App.log(result);
  return result;
}​
App.dateComparator = function (date1, date2) {
  var date1Number = App.monthToComparableNumber(date1);
  var date2Number = App.monthToComparableNumber(date2);​
  if (date1Number === null && date2Number === null) {
    return 0;
  }
  if (date1Number === null) {
    return -1;
  }
  if (date2Number === null) {
    return 1;
  }​
  return date1Number - date2Number;
}​
//Metodos auxiliares.
var Util = function ($http, $mdToast, $mdDialog) {
  serviceUtil = {};​
  serviceUtil.Alerta = function (msg, type, title, options) {
    $mdToast.show(
      $mdToast.simple()
      .textContent(msg)
      .position('top center')
      .hideDelay(3000)
    );
  }​
  serviceUtil.AlertClear = function () {
    $mdToast.hide();
  }
  serviceUtil.Confirm = function (msg, title, yesEvent, noEvent) {
    serviceUtil.AlertClear();​
    var ConfirmController = ['$scope', function ($scope) {
      $scope.data = App.confirmData;
    }];​
    App.confirmData = {};
    App.confirmData.texto = msg;
    App.confirmData.titulo = title;​​
    var confirm = $mdDialog.confirm()
      .title(App.confirmData.titulo)
      .textContent(App.confirmData.texto)
      .cancel("Não")
      .ok("Sim");​
    $mdDialog.show(confirm).then(yesEvent, noEvent);​​
  }​
  serviceUtil.delegateErro = function (erro) {
    if (erro) {
      if (erro.Message)
        serviceUtil.Alerta(erro.Message, 'error');
      else
        serviceUtil.Alerta(erro, 'error');
    }
  }​
  serviceUtil.getAppConfig = function (successEvent) {
    $http.get("GetAppConfig").success(successEvent).error(function (erro) {
      serviceUtil.delegateErro(erro);
    });
  }​
  serviceUtil.loadAppConfig = function () {​
    serviceUtil.getAppConfig(function (retorno) {
      App.globalConfig = retorno;
      //alert('carregou');
      //alert(App.globalConfig.PopUrl);
      registerCommunication(App.globalConfig.PopUrl + "/proxy.htm");
      if (window == top) {
        //top.location.replace(App.globalConfig.PopUrl);
      }​​
      resizeCaller();​
      function pageload() {
        resizeCaller();
        setTimeout(function () {
          resizeCaller();
        }, 3000);
      }​​
      $(window).load(function () {
        pageload();
      });​
    });
  }
  serviceUtil.loadAppConfig();
  serviceUtil.localeGrid = function () {
    var parametersGrid = new Array();
    parametersGrid['more'] = 'Mais';
    parametersGrid['page'] = 'Pagina';
    parametersGrid['to'] = 'a';
    parametersGrid['of'] = 'de';
    parametersGrid['first'] = '<<';
    parametersGrid['previous'] = '&nbsp;<&nbsp;';
    parametersGrid['next'] = '&nbsp;>&nbsp;';
    parametersGrid['last'] = '>>';​
    return parametersGrid;
  }​
  serviceUtil.getAutorizacoes = function (successEvent, nomeEntidade) {
    var acao = "GetAutorizacoes";
    if (nomeEntidade)
      acao = "../" + nomeEntidade + "/" + acao;​​
    $http.get(acao).success(successEvent).error(function (erro) {
      serviceUtil.delegateErro(erro);
    });
  }​
  serviceUtil.getAppConfig(function (retorno) {
    App.globalConfig = retorno;
  });​
  serviceUtil.getFiltroListas = function ($scope, successEvent, nomeEntidade) {
    var acao = "GetFiltroListas";
    if (nomeEntidade)
      acao = "../" + nomeEntidade + "/" + acao;​​
    $scope.ListaBool = [{
      Valor: true,
      Texto: 'Sim'
    }, {
      Valor: false,
      Texto: 'Não'
    }];
    $scope.ListaAtuacao = [{
      Valor: true,
      Texto: 'Positiva'
    }, {
      Valor: false,
      Texto: 'Negativa'
    }]; //{ Valor: null, Texto: 'Não Avaliada' },
    $http.get(acao).success(successEvent).error(function (erro) {
      serviceUtil.delegateErro(erro);
    });
  }​
  serviceUtil.getListasEdicao = function ($scope, successEvent, nomeEntidade) {
    $scope.ListaBool = [{
      Valor: true,
      Texto: 'Sim'
    }, {
      Valor: false,
      Texto: 'Não'
    }];
    $scope.ListaAtuacao = [{
      Valor: true,
      Texto: 'Positiva'
    }, {
      Valor: false,
      Texto: 'Negativa'
    }]; //{ Valor: null, Texto: 'Não Avaliada' }, 
    ​
    var acao = "GetListasEdicao";
    if (nomeEntidade)
      acao = "../" + nomeEntidade + "/" + acao;​
    $http.get(acao).success(successEvent).error(function (erro) {
      serviceUtil.delegateErro(erro);
    });
  }​​
  //Documentao http://www.angulargrid.com/
  serviceUtil.getDefaultGridOptions = function () {
    return {
      enableSorting: true,
      rowModelType: 'virtual', //'pagination',//decide o tipo de paginação
      maxConcurrentDatasourceRequests: 4,
      paginationInitialRowCount: 1,
      angularCompileRows: true,
      enableFilter: false,
      enableColResize: true,
      rowSelection: 'multiple',
      localeText: serviceUtil.localeGrid(),
      paginationPageSize: 20,
      pageSizeList: [10, 20, 50, 100, 150, 300, 500],
      onGridReady: function (event) {
        event.api.sizeColumnsToFit();
      }
    };
  }​
  serviceUtil.getItem = function (filtros, successo, nomeEntidade) {
    var acao = "GetItem";
    if (nomeEntidade)
      acao = "../" + nomeEntidade + "/" + acao;​
    $http.get(acao + filtros).success(successo).error(function (erro) {
      serviceUtil.delegateErro(erro);
    });
  }
  serviceUtil.getItens = function (skip, top, filtro, successo, erroDelegate, nomeEntidade) {
    var acao = "Get?skip=" + skip + "&top=" + top;
    if (nomeEntidade)
      acao = "../" + nomeEntidade + "/" + acao;​​
    $http.post(acao, filtro).success(successo).error(erroDelegate);
  }​
  serviceUtil.Atualizar = function (filtros, itemAtualizar, successo, erroEvent, nomeEntidade) {
    var acao = "Patch";
    if (nomeEntidade)
      acao = "../" + nomeEntidade + "/" + acao;​​
    $http.patch(acao + filtros, itemAtualizar).success(successo).error(function (erro) {
      serviceUtil.delegateErro(erro);
      if (erroEvent != null)
        erroEvent(erro);
    });
  }​
  serviceUtil.Inserir = function (itemInserir, successo, erroEvent, nomeEntidade) {
    var acao = "Post";
    if (nomeEntidade)
      acao = "../" + nomeEntidade + "/" + acao;​​
    $http.post(acao, itemInserir).success(successo).error(function (erro) {
      serviceUtil.delegateErro(erro);
      if (erroEvent != null) erroEvent(erro);
    });
  }​
  serviceUtil.Excluir = function (itensLista, successo, erroEvent, nomeEntidade) {
    var acao = "Excluir";
    if (nomeEntidade)
      acao = "../" + nomeEntidade + "/" + acao;
    $http.post(acao, itensLista).success(successo).error(function (erro) {
      serviceUtil.delegateErro(erro);
      if (erroEvent != null) erroEvent(erro);
    });
  }​
  serviceUtil.bindGrid = function (entidade, nomeEntidade) {
    var acao = "GetCount";
    if (nomeEntidade)
      acao = "../" + nomeEntidade + "/" + acao;​
    $http.post(acao, entidade.filtro).error(function (erro) {
        serviceUtil.delegateErro(erro);
      })
      .then(function (result) {
        entidade.gridOptions.totalLinhas = result.data;
        App.log('Qtde de regist...')
      })
  }
} */
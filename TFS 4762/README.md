#  gerador

  

*Gerado com [ng-poly](https://github.com/dustinspecker/generator-ng-poly/tree/v0.13.0) version 0.13.0*
Olá. Aqui esta um pequena lista de instruções sobre como rodar a aplicação TFS_4762
  

##  Setup

1.  Instale o [Node.js](http://nodejs.org/)

2.  No terminal, digite: `npm install -g bower gulp yo generator-ng-poly@0.13.0`
- Isso habilitará o Gulp, yeoman e o gerador globalmente
3.  Descompacte o arquivo .zip na pasta desejada.
4. Agora no terminal, va para a pasta aonde esta os arquivos.
5. digite `gulp` e aperte a tecla enter
6. O programa ira iniciar e abrirá a pagina do mesmo no Navegador
7. Caso a view Animal não abra, acesse-a por este [link](http://10.2.101.96:3000/?#/animal)
 
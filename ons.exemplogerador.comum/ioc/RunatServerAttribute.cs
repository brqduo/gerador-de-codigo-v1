﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ons.exemplogerador.comum.ioc
{
    public class RunatServerAttribute : Attribute
    {
        public virtual string Route { get; set; }
        public RunatServerAttribute()
        {

        }
    }
}

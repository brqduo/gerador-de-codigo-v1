using ons.common.data.filter;
using ons.exemplogerador.entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ons.exemplogerador.iu.web.Models
{

	/// <summary>
	/// Tabela Associativa Pessoa x Animal
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:14
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "PessoaanimalFiltro", Namespace = "http://schemas.ons.org.br/2013/04/PessoaanimalFiltro")]
	public partial class PessoaanimalFiltro : DefaultFilter<PessoaanimalFiltro, Pessoaanimal>
	{
	
		public PessoaanimalFiltro()
		{
		}


		#region "Propriedades"

		/// <summary>
		///	indicador de filtro avançado
		///</summary>
        [DataMember(Name = "FiltroAvancado")]
        public virtual bool FiltroAvancado { get; set; }

        /// <summary>
        /// Valor a ser filtrado de forma simples
        /// </summary>
        [DataMember(Name = "TextoFiltroSimples")]
        public virtual string TextoFiltroSimples { get; set; }

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "IdPessoaanimal")]
		public virtual int?  IdPessoaanimal { get; set; }
		/// <summary>
		///	Identificador da tabela de pessoas
		///</summary>
		[DataMember(Name = "IdPessoa")]
		public virtual int?  IdPessoa { get; set; }
		/// <summary>
		///	Identificador da tabela de animal
		///</summary>
		[DataMember(Name = "IdAnimal")]
		public virtual int?  IdAnimal { get; set; }
	

		#endregion
		public override void Map()
        {
            //int
            AddFilter(f => f.FiltroAvancado && f.IdPessoaanimal.HasValue, u => u.IdPessoaanimal == Filter.IdPessoaanimal);
            //int?
            AddFilter(f => f.FiltroAvancado && f.IdPessoa.HasValue, u => u.IdPessoa == Filter.IdPessoa);
            //int?
            AddFilter(f => f.FiltroAvancado && f.IdAnimal.HasValue, u => u.IdAnimal == Filter.IdAnimal);
			//Filtro simplificado
			AddFilter(f => !f.FiltroAvancado && f.TextoFiltroSimples != null & f.TextoFiltroSimples != "", u => u.Animal.NomeAnimal.Contains(Filter.TextoFiltroSimples) | u.Pessoa.NomePessoa.Contains(Filter.TextoFiltroSimples));
		}
	}
}

using ons.common.data.filter;
using ons.exemplogerador.entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ons.exemplogerador.iu.web.Models
{

	/// <summary>
	/// Tabela com o status da pessoa. Ex.: Dono, Interessado em adotar ou Em processo de adoção
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:14
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "StatuspessoaFiltro", Namespace = "http://schemas.ons.org.br/2013/04/StatuspessoaFiltro")]
	public partial class StatuspessoaFiltro : DefaultFilter<StatuspessoaFiltro, Statuspessoa>
	{
	
		public StatuspessoaFiltro()
		{
		}


		#region "Propriedades"

		/// <summary>
		///	indicador de filtro avançado
		///</summary>
        [DataMember(Name = "FiltroAvancado")]
        public virtual bool FiltroAvancado { get; set; }

        /// <summary>
        /// Valor a ser filtrado de forma simples
        /// </summary>
        [DataMember(Name = "TextoFiltroSimples")]
        public virtual string TextoFiltroSimples { get; set; }

		/// <summary>
		///	Identificador do Status da Pessoa
		///</summary>
		[DataMember(Name = "IdStatuspessoa")]
		public virtual int?  IdStatuspessoa { get; set; }
		/// <summary>
		///	Status da Pessoa
		///</summary>
		[DataMember(Name = "CodigoStatuspessoa")]
		public virtual string  CodigoStatuspessoa { get; set; }
	

		#endregion
		public override void Map()
        {
            //int
            AddFilter(f => f.FiltroAvancado && f.IdStatuspessoa.HasValue, u => u.IdStatuspessoa == Filter.IdStatuspessoa);
            //string
            AddFilter(f => f.FiltroAvancado && f.CodigoStatuspessoa != null & f.CodigoStatuspessoa != "", u => u.CodigoStatuspessoa.Contains(Filter.CodigoStatuspessoa));
			//Filtro simplificado
			AddFilter(f => !f.FiltroAvancado && f.TextoFiltroSimples != null & f.TextoFiltroSimples != "", u => u.CodigoStatuspessoa.Contains(Filter.TextoFiltroSimples));
		}
	}
}

using ons.common.data.filter;
using ons.exemplogerador.entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ons.exemplogerador.iu.web.Models
{

	/// <summary>
	/// Tabela com as informações das pessoas donas de animais
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "PessoaFiltro", Namespace = "http://schemas.ons.org.br/2013/04/PessoaFiltro")]
	public partial class PessoaFiltro : DefaultFilter<PessoaFiltro, Pessoa>
	{
	
		public PessoaFiltro()
		{
		}


		#region "Propriedades"

		/// <summary>
		///	indicador de filtro avançado
		///</summary>
        [DataMember(Name = "FiltroAvancado")]
        public virtual bool FiltroAvancado { get; set; }

        /// <summary>
        /// Valor a ser filtrado de forma simples
        /// </summary>
        [DataMember(Name = "TextoFiltroSimples")]
        public virtual string TextoFiltroSimples { get; set; }

		/// <summary>
		///	Identificador da tabela de pessoas
		///</summary>
		[DataMember(Name = "IdPessoa")]
		public virtual int?  IdPessoa { get; set; }
		/// <summary>
		///	Identificador do Status da Pessoa
		///</summary>
		[DataMember(Name = "IdStatuspessoa")]
		public virtual int?  IdStatuspessoa { get; set; }
		/// <summary>
		///	Nome da Pessoa
		///</summary>
		[DataMember(Name = "NomePessoa")]
		public virtual string  NomePessoa { get; set; }
		/// <summary>
		///	Endereço de e-mail da pessoa
		///</summary>
		[DataMember(Name = "MailPessoa")]
		public virtual string  MailPessoa { get; set; }
		/// <summary>
		///	Data de Nascimento da Pessoa
		///</summary>
		[DataMember(Name = "DataNascimento")]
		public virtual DateTime?  DataNascimento { get; set; }
		/// <summary>
		///	Código Forma de Tratamento para as pessoas. Valores possíveis: Sr. Sra. Srta.
		///</summary>
		[DataMember(Name = "CodigoFormatratamento")]
		public virtual string  CodigoFormatratamento { get; set; }
		/// <summary>
		///	Número do Cadastro de Pessoa FÍsica da Pessoa
		///</summary>
		[DataMember(Name = "NumeroCpf")]
		public virtual string  NumeroCpf { get; set; }
	

		#endregion
		public override void Map()
        {
            //int
            AddFilter(f => f.FiltroAvancado && f.IdPessoa.HasValue, u => u.IdPessoa == Filter.IdPessoa);
            //int?
            AddFilter(f => f.FiltroAvancado && f.IdStatuspessoa.HasValue, u => u.IdStatuspessoa == Filter.IdStatuspessoa);
            //string
            AddFilter(f => f.FiltroAvancado && f.NomePessoa != null & f.NomePessoa != "", u => u.NomePessoa.Contains(Filter.NomePessoa));
            //string
            AddFilter(f => f.FiltroAvancado && f.MailPessoa != null & f.MailPessoa != "", u => u.MailPessoa.Contains(Filter.MailPessoa));
            //DateTime?
            AddFilter(f => f.FiltroAvancado && f.DataNascimento.HasValue, u => u.DataNascimento == Filter.DataNascimento);
            //string
            AddFilter(f => f.FiltroAvancado && f.CodigoFormatratamento != null & f.CodigoFormatratamento != "", u => u.CodigoFormatratamento.Contains(Filter.CodigoFormatratamento));
            //string
            AddFilter(f => f.FiltroAvancado && f.NumeroCpf != null & f.NumeroCpf != "", u => u.NumeroCpf.Contains(Filter.NumeroCpf));
			//Filtro simplificado
			AddFilter(f => !f.FiltroAvancado && f.TextoFiltroSimples != null & f.TextoFiltroSimples != "", u => u.NomePessoa.Contains(Filter.TextoFiltroSimples) | u.MailPessoa.Contains(Filter.TextoFiltroSimples) | u.CodigoFormatratamento.Contains(Filter.TextoFiltroSimples) | u.NumeroCpf.Contains(Filter.TextoFiltroSimples) | u.Statuspessoa.CodigoStatuspessoa.Contains(Filter.TextoFiltroSimples));
		}
	}
}

using ons.common.data.filter;
using ons.exemplogerador.entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ons.exemplogerador.iu.web.Models
{

	/// <summary>
	/// Tabela de tipo de Animal. Ex.: Cachorro, Gato, Tartaruga, Cobra, etc.
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:14
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "TipoanimalFiltro", Namespace = "http://schemas.ons.org.br/2013/04/TipoanimalFiltro")]
	public partial class TipoanimalFiltro : DefaultFilter<TipoanimalFiltro, Tipoanimal>
	{
	
		public TipoanimalFiltro()
		{
		}


		#region "Propriedades"

		/// <summary>
		///	indicador de filtro avançado
		///</summary>
        [DataMember(Name = "FiltroAvancado")]
        public virtual bool FiltroAvancado { get; set; }

        /// <summary>
        /// Valor a ser filtrado de forma simples
        /// </summary>
        [DataMember(Name = "TextoFiltroSimples")]
        public virtual string TextoFiltroSimples { get; set; }

		/// <summary>
		///	Identificador do Tipo de Animal
		///</summary>
		[DataMember(Name = "IdTipoanimal")]
		public virtual int?  IdTipoanimal { get; set; }
		/// <summary>
		///	Nome do Tipo do Animal. Ex: Cachorro, Gato, Cobra, Réptil, etc.
		///</summary>
		[DataMember(Name = "NomeTipoanimal")]
		public virtual string  NomeTipoanimal { get; set; }
	

		#endregion
		public override void Map()
        {
            //int
            AddFilter(f => f.FiltroAvancado && f.IdTipoanimal.HasValue, u => u.IdTipoanimal == Filter.IdTipoanimal);
            //string
            AddFilter(f => f.FiltroAvancado && f.NomeTipoanimal != null & f.NomeTipoanimal != "", u => u.NomeTipoanimal.Contains(Filter.NomeTipoanimal));
			//Filtro simplificado
			AddFilter(f => !f.FiltroAvancado && f.TextoFiltroSimples != null & f.TextoFiltroSimples != "", u => u.NomeTipoanimal.Contains(Filter.TextoFiltroSimples));
		}
	}
}

using ons.common.data.filter;
using ons.exemplogerador.entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ons.exemplogerador.iu.web.Models
{

	/// <summary>
	/// Tabela com as informações dos animais
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "AnimalFiltro", Namespace = "http://schemas.ons.org.br/2013/04/AnimalFiltro")]
	public partial class AnimalFiltro : DefaultFilter<AnimalFiltro, Animal>
	{
	
		public AnimalFiltro()
		{
		}


		#region "Propriedades"

		/// <summary>
		///	indicador de filtro avançado
		///</summary>
        [DataMember(Name = "FiltroAvancado")]
        public virtual bool FiltroAvancado { get; set; }

        /// <summary>
        /// Valor a ser filtrado de forma simples
        /// </summary>
        [DataMember(Name = "TextoFiltroSimples")]
        public virtual string TextoFiltroSimples { get; set; }

		/// <summary>
		///	Identificador da tabela de animal
		///</summary>
		[DataMember(Name = "IdAnimal")]
		public virtual int?  IdAnimal { get; set; }
		/// <summary>
		///	Identificador do Tipo de Animal
		///</summary>
		[DataMember(Name = "IdTipoanimal")]
		public virtual int?  IdTipoanimal { get; set; }
		/// <summary>
		///	Nome do Animal
		///</summary>
		[DataMember(Name = "NomeAnimal")]
		public virtual string  NomeAnimal { get; set; }
		/// <summary>
		///	Data de Nascimento
		///</summary>
		[DataMember(Name = "DataNascimento")]
		public virtual DateTime?  DataNascimento { get; set; }
	

		#endregion
		public override void Map()
        {
            //int
            AddFilter(f => f.FiltroAvancado && f.IdAnimal.HasValue, u => u.IdAnimal == Filter.IdAnimal);
            //int?
            AddFilter(f => f.FiltroAvancado && f.IdTipoanimal.HasValue, u => u.IdTipoanimal == Filter.IdTipoanimal);
            //string
            AddFilter(f => f.FiltroAvancado && f.NomeAnimal != null & f.NomeAnimal != "", u => u.NomeAnimal.Contains(Filter.NomeAnimal));
            //DateTime?
            AddFilter(f => f.FiltroAvancado && f.DataNascimento.HasValue, u => u.DataNascimento == Filter.DataNascimento);
			//Filtro simplificado
			AddFilter(f => !f.FiltroAvancado && f.TextoFiltroSimples != null & f.TextoFiltroSimples != "", u => u.NomeAnimal.Contains(Filter.TextoFiltroSimples) | u.Tipoanimal.NomeTipoanimal.Contains(Filter.TextoFiltroSimples));
		}
	}
}

﻿using ons.exemplogerador.comum.ioc;
using ons.exemplogerador.iu.web.ioc;
using ons.exemplogerador.servico.ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace ons.exemplogerador.iu.web
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            if (log4net.LogManager.GetCurrentLoggers().Length == 0)
            {
                log4net.Config.XmlConfigurator.Configure();
            }

            //IoC.Initialize(new NegocioContainer(new DataConfiguration()));
            IoC.Initialize(new WebContainer());

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerActivator), new WindsorCompositionRoot(IoC.Kernel));

        }
    }
}

function Nav_swapclass(o, c1, c2) {
    var cn = o.className;
    o.className = !Nav_check(o, c1) ? cn.replace(c2, c1) : cn.replace(c1, c2);
}

function Nav_check(o, c) {
    return new RegExp('\\b' + c + '\\b').test(o.className);
}

function getURLParam(strParamName) {
    var strReturn = "";
    var strHref = window.location.href;
    if (strHref.indexOf("?") > -1) {
        var strQueryString = strHref.substr(strHref.indexOf("?"));
        var aQueryString = strQueryString.split("&");
        for (var iParam = 0; iParam < aQueryString.length; iParam++) {
            if (
                aQueryString[iParam].indexOf(strParamName + "=") > -1) {
                var aParam = aQueryString[iParam].split("=");
                strReturn = aParam[1];
                break;
            }
        }
    }
    return unescape(strReturn);
}

function interactionCompleted(functionName, params, fReturn, ifReturn) {
    var funcExists = eval('window.' + functionName) ? true : false;
    if (funcExists) {
     var myObject = (params) ? eval('(' + params + ')') : null;
     var result = window[functionName](myObject);
     if (fReturn && ifReturn) call(ifReturn, fReturn, result, null);
   }
    else
     alert('function does not exist');
}

function GetValue(object, attrName) {
    return (object instanceof Object && attrName in object) ? eval('object.' + attrName) : object;
}

var proxyUrl;

function registerCommunication(url) {
    proxyUrl = url;
}


function call(iframe, functionName, params, fReturn, ifReturn) {

    if (!proxyUrl) {
        alert('you need register iframe url first with the commmand registerCommunication');
    }

    //===POSTMESSAGE============
    if (typeof window.postMessage == 'undefined') {
        setCompletion(iframe, proxyUrl, 'proxyFrame' + iframe, functionName, params, fReturn, ifReturn);
    }
    else {
        window.parent.postMessage(functionName + '&' + params,"*");
    }
    //===POSTMESSAGE============

    //GeneriCalls
    function setCompletion(iframe, serverProxyUrl, proxyName, functionName, params, fReturn, ifReturn) {
        //Set URL and querystring
        var url = serverProxyUrl + "?f=" + functionName;
        if (params) { url += "&p=" + params; }
        if (fReturn) { url += "&r=" + fReturn; }
        if (iframe) { url += "&i=" + iframe; }
        if (ifReturn) { url += "&ir=" + ifReturn; }

        callProxy(proxyName, url);
    }

    function callProxy(id, url) {
        //Look for existing frame with name "proxyframe"
        var proxy = frames[id];
        //If the proxy iframe has already been created
        if (proxy) {
            //Redirect to the new URL
            proxy.location.href = url;
        } else {

            //Create the proxy iframe element.
            var iframe = document.createElement("iframe");
            iframe.id = id;
            iframe.name = id;
            iframe.src = url;
            iframe.style.display = "none";
            document.body.appendChild(iframe);
        }
    }
}

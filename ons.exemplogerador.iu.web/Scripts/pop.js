var Elem = ["body"];
var iframeids = [];
var autoWidth = false;
var iframehide = "yes";
var popUrl = 'http://pop.ons.org.br/pop/';

registerCommunication(popUrl + 'proxy.htm');

jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();

//if (window == top) {
//    top.location.replace(popUrl);
//}

var getFFVersion = navigator.userAgent.substring(navigator.userAgent.indexOf("Firefox")).split("/")[1];
var FFextraHeight = parseFloat(getFFVersion) >= 0.1 ? 80 : 0; //extra height in px to add to iframe in FireFox 1.0+ browsers
var FFextraWidth = 0;


function getHeight() {
    var val1 = document.body.offsetHeight;// + FFextraHeight;
    var val2 = document.body.scrollHeight;
    var retorno = 800;

    if (jQuery.browser.msie)
        retorno = (val1 > val2) ? val1 : val2;
    else
        retorno = (val1 < val2) ? val1 : val2;

    retorno += 30;

    return retorno;
}

function getWidth() {
    var val1 = document.body.offsetWidth + FFextraWidth;
    var val2 = document.body.scrollWidth;

    return ((val1 > val2) ? val1 : val2);
}


function getElementHeight(Elem) {
    var elem = document.all[Elem];
    return elem.offsetHeight + FFextraHeight;
}

function resizeCaller() {
    var h = getHeight();
    //console.log('resizeCaller');
    //console.log(h);
    call('top', 'changeHeight', h);
}

function resizeTimer() {
    resizeCaller();
    setTimeout(function () { resizeCaller(); }, 800);
}

function ajaxReturn() {
    resizeTimer();
    $('td,body,document').click(function (e) {
        resizeCaller();
    });
    $(document).ajaxSuccess(ajaxReturn);
}


function loadResize() {
    $(window).ready(resizeTimer);
    $(window).load(resizeTimer);
    $(window).resize(resizeTimer);
    $(document).ajaxSuccess(ajaxReturn);
    $(document).click(resizeTimer);
};

loadResize();
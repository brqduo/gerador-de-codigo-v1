using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ons.exemplogerador.comum.ioc;
using ons.exemplogerador.comum;
using ons.exemplogerador.comum.Reflection;
using ons.exemplogerador.servico.ioc;
using ons.exemplogerador.negocio.contrato;
using ons.exemplogerador.entidade;
using ons.exemplogerador.servico.ioc.webapi;
using ons.exemplogerador.iu.web.Models;
using System.Web.Http;
using System.Data.Entity;
using ons.common.utilities.Helper;
using System.Net.Http;
using ons.common.providers;


namespace ons.exemplogerador.iu.web.Views
{

	/// <summary>
	/// Controller API para: Tabela de tipo de Animal. Ex.: Cachorro, Gato, Tartaruga, Cobra, etc.
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:14
    /// </remarks>
	public partial class TipoanimalController : ons.exemplogerador.servico.ioc.webapi.BaseApiController
	{
        private ITipoanimalNegocio TipoanimalNegocio;
		private ons.common.providers.IHelperPOP PopHelper;

        public TipoanimalController(ons.common.providers.IHelperPOP popHelper, ITipoanimalNegocio tipoanimalNegocio)
		{
			PopHelper = popHelper;
			TipoanimalNegocio = tipoanimalNegocio;
		}

        [POPAuthorize("Consultar Tipoanimal")]
        [HttpPost]
        public IEnumerable<dynamic> Get(TipoanimalFiltro Filtro, int skip = 0, int top = 0)
        {
            return TipoanimalNegocio.ObterQuantidadeAnimalPorTipo();
        }

        [POPAuthorize("Consultar Tipoanimal")]
        [HttpPost]
        public int GetCount(TipoanimalFiltro Filtro)
        {
            return TipoanimalNegocio.Query().Where(Filtro).Count();
        }
		
        [POPCRUDAPIAuthorize()]
        public dynamic GetAutorizacoes()
        {
            return new
            {
                PodeLer = PopHelper.VerificarAcessoQualquerEscopo("Consultar Tipoanimal"),
                PodeEditar = PopHelper.VerificarAcessoQualquerEscopo("Editar Tipoanimal"),
                PodeIncluir = PopHelper.VerificarAcessoQualquerEscopo("Criar Tipoanimal"),
                PodeExcluir = PopHelper.VerificarAcessoQualquerEscopo("Excluir Tipoanimal"),
                PodeExportar = PopHelper.VerificarAcessoQualquerEscopo("Exportar Tipoanimal") 
            };
        }

		[POPCRUDAPIAuthorize()]
        public Tipoanimal GetItem(int key)
        {
		
            return TipoanimalNegocio.Query().Where(item => item.IdTipoanimal.Equals(key)).ToList().Select(a => new Tipoanimal() { IdTipoanimal = a.IdTipoanimal, NomeTipoanimal = a.NomeTipoanimal }).FirstOrDefault();
        }
		private Tipoanimal ObterItem(int key)
        {
            return TipoanimalNegocio.Query().Where(item => item.IdTipoanimal.Equals(key)).FirstOrDefault();
        }
		
		[POPCRUDAPIAuthorize()]
        public dynamic GetFiltroListas()
        {
            return new {  };
        }
		
		[POPCRUDAPIAuthorize()]
        public dynamic GetListasEdicao()
        {
            return new {  };
        }
		
		        // POST: odata/Tipoanimal
        [POPCRUDAPIAuthorize]
        public dynamic Post([FromBody]Tipoanimal tipoanimal)
        {
			TipoanimalNegocio.Criar(tipoanimal);
            TipoanimalNegocio.Salvar();
			return new { Sucesso = true, Mensagem = "Salvo com sucesso." };
        }

        // PATCH: odata/Tipoanimal
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public dynamic Patch(int key, [FromBody]Tipoanimal tipoanimal)
        {
			Tipoanimal original = ObterItem(key);
			ReflectionUtil.Fill(tipoanimal, original);
			
			TipoanimalNegocio.Atualizar(original);
            TipoanimalNegocio.Salvar();

			return new { Sucesso = true, Mensagem = "Atualizado com sucesso." };
        }

        // DELETE: odata/Tipoanimal
        [POPCRUDAPIAuthorize]
        [HttpPost]
        public dynamic Excluir(dynamic[] tipoanimalLista)
        {
            if (tipoanimalLista != null && tipoanimalLista.Length > 0)
            {
                foreach (var item in tipoanimalLista)
                {
                    Tipoanimal original = ObterItem((int)item.IdTipoanimal);
                    TipoanimalNegocio.Excluir(original);
                }
                TipoanimalNegocio.Salvar();

                return new { Sucesso = true, Mensagem = "Excluido com sucesso." };
            }
            else
            {
                return new { Sucesso = false, Mensagem = "Informe algum item para excluir." };
            }
        }
		
		protected override void Dispose(bool disposing)
        {
            TipoanimalNegocio = null;
            base.Dispose(disposing);
        }

	}
}

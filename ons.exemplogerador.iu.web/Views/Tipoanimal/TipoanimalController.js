
//Controller Tipoanimal
App.controller("TipoanimalController", function ($scope, $rootScope, $http, Util) {
	$scope.tipoanimal = {};
	Util.getListasEdicao($scope, function (resultado) { $scope.tipoanimal.listasEdicao = resultado; ObterItem(); });

    function ObterItem()
	{
        var filtro = App.tipoanimal.filtroSelectedItem();

        if (filtro) {
			Util.getItem(App.tipoanimal.filtroSelectedItem(), function (item) { $scope.tipoanimal.itemEdicao = item; });
        }
        else
        {
            $scope.tipoanimal.itemEdicao = {};
        }
    }

    $scope.Salvar = function () {
        var filtro = App.tipoanimal.filtroSelectedItem();

		if (filtro) {
            Util.Atualizar(filtro, $scope.tipoanimal.itemEdicao, ValidarRetornoSalvar);
        }
        else {
        	$scope.tipoanimal.itemEdicao.IdTipoanimal = 0;

            Util.Inserir($scope.tipoanimal.itemEdicao, ValidarRetornoSalvar);
        }
    }

    function ValidarRetornoSalvar(retorno)
    {
        Util.Alerta(retorno.Mensagem, retorno.Sucesso);

        if (retorno.Sucesso === true) {
            App.tipoanimal.FecharEdicao();
        }
    }
});

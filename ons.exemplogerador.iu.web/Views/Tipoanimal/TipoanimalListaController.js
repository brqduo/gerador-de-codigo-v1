
//Controller Tipoanimal
App.controller("TipoanimalListaController", function ($scope, $rootScope, $http, Util) {
    //Inicialização.
    $scope.tipoanimal = {};
    $scope.tipoanimal.filtro = {};
    $scope.tipoanimal.filtroListas = {};
    App.tipoanimal = {};

    Util.getAutorizacoes(function (retorno) { $scope.permissao = retorno; })

    Util.getFiltroListas($scope, function (resultado) { $scope.tipoanimal.filtroListas = resultado; });

	App.tipoanimal.columnDefinitions = function () {
		var larguraTela = (getWidth() - 80);
		var largura = larguraTela * 1;
		
		if(largura < 150)
		{
			largura = 150;
		}
		
		var columnDefs = [
			// index da linha
			//{
			//    displayName: "#", width: 50, cellRenderer: function (params) {
			//        return params.node.id + 1;
			//    }
			//},
          	{ headerName: "Nome", field: "NomeTipoanimal", width: largura/2, checkboxSelection: true },
          	{ headerName: "Quantidade", field: "Quantidade", width: largura/2 }
    	];
	    return columnDefs;
	}
	
	App.tipoanimal.columnExport = function () {
		var columnEx = 'IFNULL(NomeTipoanimal, \'\') AS [Nome]';
	    return columnEx;
	}

	App.tipoanimal.Exportar = function () {
		Util.getItens(0, 0, $scope.tipoanimal.filtro, 
				function(listaRetorno){Util.ExportarExcel(listaRetorno,'tipoanimal', App.tipoanimal.columnExport())}, 
				function (erro) { serviceUtil.delegateErro(erro); })
	}

    //Documentação https://www.ag-grid.com/
    $scope.tipoanimal.gridOptions = Util.getDefaultGridOptions();
    $scope.tipoanimal.gridOptions.columnDefs = App.tipoanimal.columnDefinitions();

    Util.bindGrid($scope.tipoanimal);

    App.tipoanimal.filtroSelectedItem = function () {
		if ($scope.tipoanimal.gridOptions.api.getSelectedRows().length > 0) {
            return encodeURI('?key=' + $scope.tipoanimal.gridOptions.api.getSelectedRows()[0].IdTipoanimal);
        }
        else
            return null;
    }

    App.tipoanimal.AbrirEdicao = function () {
		if ($scope.tipoanimal.gridOptions.api.getSelectedRows().length == 0) {
            Util.Alerta('Selecione um item para edição.', 'warning');
            return;
        }

        if($scope.tipoanimal.gridOptions.api.getSelectedRows().length > 1){
            Util.Alerta('Selecione apenas um item para edição.', 'warning');
            return;
        }
		Util.AlertClear();
        //ngDialog.open({ closeByDocument: false, template: 'Tipoanimal.html', controller: 'TipoanimalController' });

    }
	
    App.tipoanimal.FecharEdicao = function () {
        //ngDialog.closeAll();
        $scope.Pesquisar();
    }

    App.tipoanimal.AbrirInclusao = function () {
	    $scope.tipoanimal.gridOptions.api.deselectAll();
		Util.AlertClear();
	    //ngDialog.open({ closeByDocument: false, template: 'Tipoanimal.html', controller: 'TipoanimalController' });
	}

    $scope.Pesquisar = function () {
         Util.bindGrid($scope.tipoanimal);
    }

    App.tipoanimal.Excluir = function (successo) {
		if ($scope.tipoanimal.gridOptions.api.getSelectedRows().length == 0) {
			Util.Alerta('Selecione algum item para exclusão.', 'warning');
			return;
		}
		
		Util.Confirm('Deseja realmente excluir este item?', 'Exclusão', function () {
			var tipoanimalLista = [];

			for (var i = 0; i < $scope.tipoanimal.gridOptions.api.getSelectedRows().length; i++) {
				tipoanimalLista.push({ "IdTipoanimal": $scope.tipoanimal.gridOptions.api.getSelectedRows()[i].IdTipoanimal });
			}

			Util.Excluir(tipoanimalLista, successo);
		});
	}

	$scope.Editar = App.tipoanimal.AbrirEdicao;
	$scope.Incluir = App.tipoanimal.AbrirInclusao;
	$scope.Exportar = App.tipoanimal.Exportar;
    
	$scope.Apagar = function () {
	    App.tipoanimal.Excluir(ValidarRetornoExclusao);
	}

	function ValidarRetornoExclusao(retorno) {
	    Util.Alerta(retorno.Mensagem, retorno.Sucesso);

	    if (retorno.Sucesso === true) {
    	    $scope.Pesquisar();
	    }
	}
});



using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ons.exemplogerador.comum.ioc;
using ons.exemplogerador.comum;
using ons.exemplogerador.comum.Reflection;
using ons.exemplogerador.servico.ioc;
using ons.exemplogerador.negocio.contrato;
using ons.exemplogerador.entidade;
using ons.exemplogerador.servico.ioc.webapi;
using ons.exemplogerador.iu.web.Models;
using System.Web.Http;
using System.Data.Entity;
using ons.common.utilities.Helper;
using System.Net.Http;
using ons.common.providers;


namespace ons.exemplogerador.iu.web.Views
{

	/// <summary>
	/// Controller API para: Tabela com as informações das pessoas donas de animais
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	public partial class PessoaController : ons.exemplogerador.servico.ioc.webapi.BaseApiController
	{
        private IPessoaNegocio PessoaNegocio;
		private ons.common.providers.IHelperPOP PopHelper;
		private IStatuspessoaNegocio StatuspessoaNegocio;

        public PessoaController(ons.common.providers.IHelperPOP popHelper, IPessoaNegocio pessoaNegocio, IStatuspessoaNegocio statuspessoaNegocio)
		{
			PopHelper = popHelper;
			PessoaNegocio = pessoaNegocio;
			StatuspessoaNegocio = statuspessoaNegocio;
		}

        [POPAuthorize("Consultar Pessoa")]
        [HttpPost]
        public IEnumerable<dynamic> Get(PessoaFiltro Filtro, int skip = 0, int top = 0)
        {
            List<Pessoa> listaPessoas = new List<Pessoa>();
            Pessoa pessoa = new Pessoa() {
                IdPessoa = 1,
                Statuspessoa = new Statuspessoa()
                {
                    IdStatuspessoa = 1,
                    CodigoStatuspessoa = "Ativo"
                },
                IdStatuspessoa = 1,
                CodigoFormatratamento = "123",
                DataNascimento = DateTime.Now,
                NomePessoa = "Carlos Furtado",
                MailPessoa = "carlosfurtado@brq.com",
                NumeroCpf = "000.000.000-00"
            };
            listaPessoas.Add(pessoa);

            return listaPessoas.AsQueryable<Pessoa>().Where(Filtro).Select(x => new {
                IdPessoa = x.IdPessoa,
                Statuspessoa = x.Statuspessoa.CodigoStatuspessoa,
                x.NomePessoa,
                x.MailPessoa,
                DataNascimento = x.DataNascimento.Value.ToString("dd/MM/yyyy"),
                x.CodigoFormatratamento,
                x.NumeroCpf
            }).ToList();
        }

        [POPAuthorize("Consultar Pessoa")]
        [HttpPost]
        public int GetCount(PessoaFiltro Filtro)
        {
            return Get(Filtro).Count();
        }
		
        [POPCRUDAPIAuthorize()]
        public dynamic GetAutorizacoes()
        {
            return new
            {
                PodeLer = PopHelper.VerificarAcessoQualquerEscopo("Consultar Pessoa"),
                PodeEditar = PopHelper.VerificarAcessoQualquerEscopo("Editar Pessoa"),
                PodeIncluir = PopHelper.VerificarAcessoQualquerEscopo("Criar Pessoa"),
                PodeExcluir = PopHelper.VerificarAcessoQualquerEscopo("Excluir Pessoa"),
                PodeExportar = PopHelper.VerificarAcessoQualquerEscopo("Exportar Pessoa") 
            };
        }

		[POPCRUDAPIAuthorize()]
        public Pessoa GetItem(int key)
        {
            List<Pessoa> listaPessoas = new List<Pessoa>();
            Pessoa pessoa = new Pessoa()
            {
                IdPessoa = 1,
                Statuspessoa = new Statuspessoa()
                {
                    IdStatuspessoa = 1,
                    CodigoStatuspessoa = "Ativo"
                },
                IdStatuspessoa = 1,
                CodigoFormatratamento = "123",
                DataNascimento = DateTime.Now,
                NomePessoa = "Carlos Furtado",
                MailPessoa = "carlosfurtado@brq.com",
                NumeroCpf = "000.000.000-00"
            };
            listaPessoas.Add(pessoa);

            return listaPessoas.Where(item => item.IdPessoa.Equals(key)).ToList().FirstOrDefault();
        }
		private Pessoa ObterItem(int key)
        {
            return PessoaNegocio.Query().Where(item => item.IdPessoa.Equals(key)).FirstOrDefault();
        }
		
		[POPCRUDAPIAuthorize()]
        public dynamic GetFiltroListas()
        {
            List<Statuspessoa> lista = new List<Statuspessoa>();
            Statuspessoa statuspessoa1 = new Statuspessoa()
            {
                IdStatuspessoa = 1,
                CodigoStatuspessoa = "Ativo"
            };
            Statuspessoa statuspessoa2 = new Statuspessoa()
            {
                IdStatuspessoa = 2,
                CodigoStatuspessoa = "Inativo"
            };

            lista.Add(statuspessoa1);
            lista.Add(statuspessoa2);

            return new
            {
                StatuspessoaLista = lista.OrderBy(a => a.CodigoStatuspessoa).Select(a => new { Valor = a.IdStatuspessoa, Texto = a.CodigoStatuspessoa }).ToList()
            };
        }
		
		[POPCRUDAPIAuthorize()]
        public dynamic GetListasEdicao()
        {
            List<Statuspessoa> lista = new List<Statuspessoa>();
            Statuspessoa statuspessoa1 = new Statuspessoa()
            {
                IdStatuspessoa = 1,
                CodigoStatuspessoa = "Ativo"
            };
            Statuspessoa statuspessoa2 = new Statuspessoa()
            {
                IdStatuspessoa = 2,
                CodigoStatuspessoa = "Inativo"
            };

            lista.Add(statuspessoa1);
            lista.Add(statuspessoa2);

            return new
            {
                StatuspessoaLista = lista.OrderBy(a=> a.CodigoStatuspessoa).Select(a => new { Valor = a.IdStatuspessoa, Texto = a.CodigoStatuspessoa}).ToList()
            };
        }
		
		        // POST: odata/Pessoa
        [POPCRUDAPIAuthorize]
        public dynamic Post([FromBody]Pessoa pessoa)
        {
			PessoaNegocio.Criar(pessoa);
            PessoaNegocio.Salvar();
			return new { Sucesso = true, Mensagem = "Salvo com sucesso." };
        }

        // PATCH: odata/Pessoa
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public dynamic Patch(int key, [FromBody]Pessoa pessoa)
        {
			Pessoa original = ObterItem(key);
			ReflectionUtil.Fill(pessoa, original);
			
			PessoaNegocio.Atualizar(original);
            PessoaNegocio.Salvar();

			return new { Sucesso = true, Mensagem = "Atualizado com sucesso." };
        }

        // DELETE: odata/Pessoa
        [POPCRUDAPIAuthorize]
        [HttpPost]
        public dynamic Excluir(dynamic[] pessoaLista)
        {
            if (pessoaLista != null && pessoaLista.Length > 0)
            {
                foreach (var item in pessoaLista)
                {
                    Pessoa original = ObterItem((int)item.IdPessoa);
                    PessoaNegocio.Excluir(original);
                }
                PessoaNegocio.Salvar();

                return new { Sucesso = true, Mensagem = "Excluido com sucesso." };
            }
            else
            {
                return new { Sucesso = false, Mensagem = "Informe algum item para excluir." };
            }
        }
		
		protected override void Dispose(bool disposing)
        {
            PessoaNegocio = null;
			StatuspessoaNegocio = null;
            base.Dispose(disposing);
        }

	}
}

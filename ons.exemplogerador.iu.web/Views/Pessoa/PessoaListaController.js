//Controller Pessoa
App.controller("PessoaListaController", function ($scope, $rootScope, $http, Util, $mdDialog) {
    //Inicialização.
    $scope.pessoa = {};
    $scope.pessoa.filtro = {};
    $scope.pessoa.filtroListas = {};
    App.pessoa = {};

    Util.getAutorizacoes(function (retorno) {
        $scope.permissao = retorno;
    });

    Util.getFiltroListas($scope, function (resultado) {
        $scope.pessoa.filtroListas = resultado;
    });

	App.pessoa.columnDefinitions = function () {
		var larguraTela = (getWidth() - 80);
		var largura = larguraTela * 0.166666666666667;
		
		if(largura < 150)
		{
			largura = 150;
		}
		
		var columnDefs = [
			// index da linha
			//{
			//    displayName: "#", width: 50, cellRenderer: function (params) {
			//        return params.node.id + 1;
			//    }
			//},
          	{ headerName: "Status pessoa", field: "Statuspessoa", width: largura, checkboxSelection: true }
        	, { headerName: "Nome", field: "NomePessoa", width: largura }
        	, { headerName: "E-mail", field: "MailPessoa", width: largura }
        	, { headerName: "Data Nascimento", field: "DataNascimento", width: largura }
        	, { headerName: "Código Formatratamento", field: "CodigoFormatratamento", width: largura }
        	, { headerName: "Número Cpf", field: "NumeroCpf", width: largura }
    	];
	    return columnDefs;
	}
	
	App.pessoa.columnExport = function () {
		var columnEx = 'IFNULL(Statuspessoa, \'\') AS [Status pessoa], IFNULL(NomePessoa, \'\') AS [Nome], IFNULL(MailPessoa, \'\') AS [E-mail], IFNULL(DataNascimento, \'\') AS [Data Nascimento], IFNULL(CodigoFormatratamento, \'\') AS [Código Formatratamento], IFNULL(NumeroCpf, \'\') AS [Número Cpf]';
	    return columnEx;
	}

    App.pessoa.Exportar = function () {
        Util.getItens(0, 0, $scope.pessoa.filtro,
            function (listaRetorno) {
                Util.ExportarExcel(listaRetorno, 'pessoa', App.pessoa.columnExport())
            },
            function (erro) {
                serviceUtil.delegateErro(erro);
            })
    }

    //Documentação https://www.ag-grid.com/
    $scope.pessoa.gridOptions = Util.getDefaultGridOptions();
    $scope.pessoa.gridOptions.columnDefs = App.pessoa.columnDefinitions();

    Util.bindGrid($scope.pessoa);

    App.pessoa.filtroSelectedItem = function () {
		if ($scope.pessoa.gridOptions.api.getSelectedRows().length > 0) {
            return encodeURI('?key=' + $scope.pessoa.gridOptions.api.getSelectedRows()[0].IdPessoa);
        }
        else
            return null;
    }

    App.pessoa.AbrirEdicao = function (ev) {
        if ($scope.pessoa.gridOptions.api.getSelectedRows().length == 0) {
            Util.Alerta('Selecione um item para edição.', 'warning');
            return;
        }

        if ($scope.pessoa.gridOptions.api.getSelectedRows().length > 1) {
            Util.Alerta('Selecione apenas um item para edição.', 'warning');
            return;
        }
        Util.AlertClear();

        $mdDialog.show({
            controller: EditarController,
            templateUrl: 'Pessoa.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: $scope.customFullscreen
        });
    }

    App.pessoa.FecharEdicao = function () {
        Util.FecharModal();
        $scope.Pesquisar();
    }

    App.pessoa.AbrirInclusao = function (ev) {
        $scope.pessoa.gridOptions.api.deselectAll();
        Util.AlertClear();

        $mdDialog.show({
            controller: EditarController,
            templateUrl: 'Pessoa.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: $scope.customFullscreen
        });
    }

    $scope.Pesquisar = function () {
        Util.bindGrid($scope.pessoa);
    }

    App.pessoa.Excluir = function (successo) {
        if ($scope.pessoa.gridOptions.api.getSelectedRows().length == 0) {
            Util.Alerta('Selecione algum item para exclusão.', 'warning');
            return;
        }

        Util.Confirm('Deseja realmente excluir este item?', 'Exclusão', function () {
            var pessoaLista = [];

            for (var i = 0; i < $scope.pessoa.gridOptions.api.getSelectedRows().length; i++) {
                pessoaLista.push({ "IdPessoa": $scope.pessoa.gridOptions.api.getSelectedRows()[i].IdPessoa });
            }

            Util.Excluir(pessoaLista, successo);
        });
    }

    $scope.Editar = App.pessoa.AbrirEdicao;
    $scope.Incluir = App.pessoa.AbrirInclusao;
    $scope.Exportar = App.pessoa.Exportar;

    $scope.Apagar = function () {
        App.pessoa.Excluir(ValidarRetornoExclusao);
    }

    function ValidarRetornoExclusao(retorno) {
        Util.Alerta(retorno.Mensagem, retorno.Sucesso);

        if (retorno.Sucesso === true) {
            $scope.Pesquisar();
        }
    };

    var EditarController = function ($scope, Util) {
        $scope.pessoa = {};
        $scope.pessoa.listasEdicao = {};

        $scope.hide = function () {
            $mdDialog.hide();
        };

        $scope.cancel = function () {
            $mdDialog.cancel();
        };

        $scope.answer = function (answer) {
            $mdDialog.hide(answer);
        };

        Util.getListasEdicao($scope, function (resultado) {
            $scope.pessoa.itemEdicaoListas = resultado;
            obterItem();
        });

        function obterItem() {
            var filtro = App.pessoa.filtroSelectedItem();

            if (filtro) {
                $scope.pessoa.alterar = true;
                Util.getItem(App.pessoa.filtroSelectedItem(), function (item) {
                    $scope.pessoa.itemEdicao = item;
                    $scope.pessoa.itemEdicao.DataNascimento = new Date(item.DataNascimento);
                });
            }
            else {
                $scope.pessoa.alterar = false;
                $scope.pessoa.itemEdicao = {};
                $scope.pessoa.itemEdicao.FlgCadastrado = false;
                $scope.pessoa.itemEdicao.FlgCancelarMonitoramento = false;
            }
        }

        $scope.Salvar = function () {
            var filtro = App.pessoa.filtroSelectedItem();

            if (filtro) {
                Util.Atualizar(filtro, $scope.pessoa.itemEdicao, validarRetornoSalvar);
            }
            else {
                Util.Inserir($scope.pessoa.itemEdicao, validarRetornoSalvar);
            }
        }

        function validarRetornoSalvar(retorno) {
            Util.Alerta(retorno.Mensagem, retorno.Sucesso);
            if (retorno.Sucesso === true) {
                App.pessoa.FecharEdicao();
            }
        }

        $scope.closeAll = function () {
            Util.FecharModal();
        };
    };

});
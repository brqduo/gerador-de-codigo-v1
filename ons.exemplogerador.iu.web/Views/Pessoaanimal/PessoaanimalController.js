
//Controller Pessoaanimal
App.controller("PessoaanimalController", function ($scope, $rootScope, $http, Util) {
	$scope.pessoaanimal = {};
	Util.getListasEdicao($scope, function (resultado) { $scope.pessoaanimal.listasEdicao = resultado; ObterItem(); });

    function ObterItem()
	{
        var filtro = App.pessoaanimal.filtroSelectedItem();

        if (filtro) {
			Util.getItem(App.pessoaanimal.filtroSelectedItem(), function (item) { $scope.pessoaanimal.itemEdicao = item; });
        }
        else
        {
            $scope.pessoaanimal.itemEdicao = {};
        }
    }

    $scope.Salvar = function () {
        var filtro = App.pessoaanimal.filtroSelectedItem();

		if (filtro) {
            Util.Atualizar(filtro, $scope.pessoaanimal.itemEdicao, ValidarRetornoSalvar);
        }
        else {
        	$scope.pessoaanimal.itemEdicao.IdPessoaanimal = 0;

            Util.Inserir($scope.pessoaanimal.itemEdicao, ValidarRetornoSalvar);
        }
    }

    function ValidarRetornoSalvar(retorno)
    {
        Util.Alerta(retorno.Mensagem, retorno.Sucesso);

        if (retorno.Sucesso === true) {
            App.pessoaanimal.FecharEdicao();
        }
    }
});

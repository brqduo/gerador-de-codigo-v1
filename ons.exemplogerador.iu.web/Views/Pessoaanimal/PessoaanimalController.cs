using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ons.exemplogerador.comum.ioc;
using ons.exemplogerador.comum;
using ons.exemplogerador.comum.Reflection;
using ons.exemplogerador.servico.ioc;
using ons.exemplogerador.negocio.contrato;
using ons.exemplogerador.entidade;
using ons.exemplogerador.servico.ioc.webapi;
using ons.exemplogerador.iu.web.Models;
using System.Web.Http;
using System.Data.Entity;
using ons.common.utilities.Helper;
using System.Net.Http;
using ons.common.providers;


namespace ons.exemplogerador.iu.web.Views
{

	/// <summary>
	/// Controller API para: Tabela Associativa Pessoa x Animal
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	public partial class PessoaanimalController : ons.exemplogerador.servico.ioc.webapi.BaseApiController
	{
        private IPessoaanimalNegocio PessoaanimalNegocio;
		private ons.common.providers.IHelperPOP PopHelper;
		private IAnimalNegocio AnimalNegocio;
		private IPessoaNegocio PessoaNegocio;

        public PessoaanimalController(ons.common.providers.IHelperPOP popHelper, IPessoaanimalNegocio pessoaanimalNegocio, IAnimalNegocio animalNegocio, IPessoaNegocio pessoaNegocio)
		{
			PopHelper = popHelper;
			PessoaanimalNegocio = pessoaanimalNegocio;
			AnimalNegocio = animalNegocio;
			PessoaNegocio = pessoaNegocio;
		}

        [POPAuthorize("Consultar Pessoaanimal")]
        [HttpPost]
        public IEnumerable<dynamic> Get(PessoaanimalFiltro Filtro, int skip = 0, int top = 0)
        {
            return PessoaanimalNegocio.Query().Include(a=> a.Animal).Include(a=> a.Pessoa).Where(Filtro).Page(skip, top).Select(a=> new { a.IdPessoaanimal, a.IdPessoa, a.IdAnimal, Animal = a.IdAnimal == null ? null : a.Animal.NomeAnimal, Pessoa = a.IdPessoa == null ? null : a.Pessoa.NomePessoa }).ToList().Select(a=> new { a.IdPessoaanimal, a.IdPessoa, a.IdAnimal, a.Animal, a.Pessoa }).ToList();
        }

        [POPAuthorize("Consultar Pessoaanimal")]
        [HttpPost]
        public int GetCount(PessoaanimalFiltro Filtro)
        {
            return PessoaanimalNegocio.Query().Where(Filtro).Count();
        }
		
        [POPCRUDAPIAuthorize()]
        public dynamic GetAutorizacoes()
        {
            return new
            {
                PodeLer = PopHelper.VerificarAcessoQualquerEscopo("Consultar Pessoaanimal"),
                PodeEditar = PopHelper.VerificarAcessoQualquerEscopo("Editar Pessoaanimal"),
                PodeIncluir = PopHelper.VerificarAcessoQualquerEscopo("Criar Pessoaanimal"),
                PodeExcluir = PopHelper.VerificarAcessoQualquerEscopo("Excluir Pessoaanimal"),
                PodeExportar = PopHelper.VerificarAcessoQualquerEscopo("Exportar Pessoaanimal") 
            };
        }

		[POPCRUDAPIAuthorize()]
        public Pessoaanimal GetItem(int key)
        {
		
            return PessoaanimalNegocio.Query().Where(item => item.IdPessoaanimal.Equals(key)).ToList().Select(a => new Pessoaanimal() { IdPessoaanimal = a.IdPessoaanimal, IdPessoa = a.IdPessoa, IdAnimal = a.IdAnimal }).FirstOrDefault();
        }
		private Pessoaanimal ObterItem(int key)
        {
            return PessoaanimalNegocio.Query().Where(item => item.IdPessoaanimal.Equals(key)).FirstOrDefault();
        }
		
		[POPCRUDAPIAuthorize()]
        public dynamic GetFiltroListas()
        {
            return new { AnimalLista = AnimalNegocio.Query().OrderBy(a=> a.NomeAnimal).Select(a => new { Valor = a.IdAnimal, Texto = a.NomeAnimal}).ToList(), PessoaLista = PessoaNegocio.Query().OrderBy(a=> a.NomePessoa).Select(a => new { Valor = a.IdPessoa, Texto = a.NomePessoa}).ToList() };
        }
		
		[POPCRUDAPIAuthorize()]
        public dynamic GetListasEdicao()
        {
            return new { AnimalLista = AnimalNegocio.Query().OrderBy(a=> a.NomeAnimal).Select(a => new { Valor = a.IdAnimal, Texto = a.NomeAnimal}).ToList(), PessoaLista = PessoaNegocio.Query().OrderBy(a=> a.NomePessoa).Select(a => new { Valor = a.IdPessoa, Texto = a.NomePessoa}).ToList() };
        }
		
		        // POST: odata/Pessoaanimal
        [POPCRUDAPIAuthorize]
        public dynamic Post([FromBody]Pessoaanimal pessoaanimal)
        {
			PessoaanimalNegocio.Criar(pessoaanimal);
            PessoaanimalNegocio.Salvar();
			return new { Sucesso = true, Mensagem = "Salvo com sucesso." };
        }

        // PATCH: odata/Pessoaanimal
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public dynamic Patch(int key, [FromBody]Pessoaanimal pessoaanimal)
        {
			Pessoaanimal original = ObterItem(key);
			ReflectionUtil.Fill(pessoaanimal, original);
			
			PessoaanimalNegocio.Atualizar(original);
            PessoaanimalNegocio.Salvar();

			return new { Sucesso = true, Mensagem = "Atualizado com sucesso." };
        }

        // DELETE: odata/Pessoaanimal
        [POPCRUDAPIAuthorize]
        [HttpPost]
        public dynamic Excluir(dynamic[] pessoaanimalLista)
        {
            if (pessoaanimalLista != null && pessoaanimalLista.Length > 0)
            {
                foreach (var item in pessoaanimalLista)
                {
                    Pessoaanimal original = ObterItem((int)item.IdPessoaanimal);
                    PessoaanimalNegocio.Excluir(original);
                }
                PessoaanimalNegocio.Salvar();

                return new { Sucesso = true, Mensagem = "Excluido com sucesso." };
            }
            else
            {
                return new { Sucesso = false, Mensagem = "Informe algum item para excluir." };
            }
        }
		
		protected override void Dispose(bool disposing)
        {
            PessoaanimalNegocio = null;
			AnimalNegocio = null;
			PessoaNegocio = null;
            base.Dispose(disposing);
        }

	}
}

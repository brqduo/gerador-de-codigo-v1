
//Controller Pessoaanimal
App.controller("PessoaanimalListaController", function ($scope, $rootScope, $http, Util) {
    //Inicialização.
    $scope.pessoaanimal = {};
    $scope.pessoaanimal.filtro = {};
    $scope.pessoaanimal.filtroListas = {};
    App.pessoaanimal = {};

    Util.getAutorizacoes(function (retorno) { $scope.permissao = retorno; })

    Util.getFiltroListas($scope, function (resultado) { $scope.pessoaanimal.filtroListas = resultado; });

	App.pessoaanimal.columnDefinitions = function () {
		var larguraTela = (getWidth() - 80);
		var largura = larguraTela * 0.5;
		
		if(largura < 150)
		{
			largura = 150;
		}
		
		var columnDefs = [
			// index da linha
			//{
			//    displayName: "#", width: 50, cellRenderer: function (params) {
			//        return params.node.id + 1;
			//    }
			//},
          	{ headerName: "Animal", field: "Animal", width: largura, checkboxSelection: true }
        	, { headerName: "Pessoa", field: "Pessoa", width: largura }
    	];
	    return columnDefs;
	}
	
	App.pessoaanimal.columnExport = function () {
		var columnEx = 'IFNULL(Animal, \'\') AS [Animal], IFNULL(Pessoa, \'\') AS [Pessoa]';
	    return columnEx;
	}

	App.pessoaanimal.Exportar = function () {
		Util.getItens(0, 0, $scope.pessoaanimal.filtro, 
				function(listaRetorno){Util.ExportarExcel(listaRetorno,'pessoaanimal', App.pessoaanimal.columnExport())}, 
				function (erro) { serviceUtil.delegateErro(erro); })
	}

    //Documentação https://www.ag-grid.com/
    $scope.pessoaanimal.gridOptions = Util.getDefaultGridOptions();
    $scope.pessoaanimal.gridOptions.columnDefs = App.pessoaanimal.columnDefinitions();

    Util.bindGrid($scope.pessoaanimal);

    App.pessoaanimal.filtroSelectedItem = function () {
		if ($scope.pessoaanimal.gridOptions.api.getSelectedRows().length > 0) {
            return encodeURI('?key=' + $scope.pessoaanimal.gridOptions.api.getSelectedRows()[0].IdPessoaanimal);
        }
        else
            return null;
    }

    App.pessoaanimal.AbrirEdicao = function () {
		if ($scope.pessoaanimal.gridOptions.api.getSelectedRows().length == 0) {
            Util.Alerta('Selecione um item para edição.', 'warning');
            return;
        }

        if($scope.pessoaanimal.gridOptions.api.getSelectedRows().length > 1){
            Util.Alerta('Selecione apenas um item para edição.', 'warning');
            return;
        }
		Util.AlertClear();
        //ngDialog.open({ closeByDocument: false, template: 'Pessoaanimal.html', controller: 'PessoaanimalController' });

    }
	
    App.pessoaanimal.FecharEdicao = function () {
        //ngDialog.closeAll();
        $scope.Pesquisar();
    }

    App.pessoaanimal.AbrirInclusao = function () {
	    $scope.pessoaanimal.gridOptions.api.deselectAll();
		Util.AlertClear();
	    //ngDialog.open({ closeByDocument: false, template: 'Pessoaanimal.html', controller: 'PessoaanimalController' });
	}

    $scope.Pesquisar = function () {
         Util.bindGrid($scope.pessoaanimal);
    }

    App.pessoaanimal.Excluir = function (successo) {
		if ($scope.pessoaanimal.gridOptions.api.getSelectedRows().length == 0) {
			Util.Alerta('Selecione algum item para exclusão.', 'warning');
			return;
		}
		
		Util.Confirm('Deseja realmente excluir este item?', 'Exclusão', function () {
			var pessoaanimalLista = [];

			for (var i = 0; i < $scope.pessoaanimal.gridOptions.api.getSelectedRows().length; i++) {
				pessoaanimalLista.push({ "IdPessoaanimal": $scope.pessoaanimal.gridOptions.api.getSelectedRows()[i].IdPessoaanimal });
			}

			Util.Excluir(pessoaanimalLista, successo);
		});
	}

	$scope.Editar = App.pessoaanimal.AbrirEdicao;
	$scope.Incluir = App.pessoaanimal.AbrirInclusao;
	$scope.Exportar = App.pessoaanimal.Exportar;
    
	$scope.Apagar = function () {
	    App.pessoaanimal.Excluir(ValidarRetornoExclusao);
	}

	function ValidarRetornoExclusao(retorno) {
	    Util.Alerta(retorno.Mensagem, retorno.Sucesso);

	    if (retorno.Sucesso === true) {
    	    $scope.Pesquisar();
	    }
	}
});



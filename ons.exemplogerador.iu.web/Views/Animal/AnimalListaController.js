
//Controller Animal
App.controller("AnimalListaController", function ($scope, $rootScope, $http, Util) {
    //Inicialização.
    $scope.animal = {};
    $scope.animal.filtro = {};
    $scope.animal.filtroListas = {};
    App.animal = {};

    Util.getAutorizacoes(function (retorno) { $scope.permissao = retorno; })

    Util.getFiltroListas($scope, function (resultado) { $scope.animal.filtroListas = resultado; });

	App.animal.columnDefinitions = function () {
		var larguraTela = (getWidth() - 80);
		var largura = larguraTela * 0.333333333333333;
		
		if(largura < 150)
		{
			largura = 150;
		}
		
		var columnDefs = [
			// index da linha
			//{
			//    displayName: "#", width: 50, cellRenderer: function (params) {
			//        return params.node.id + 1;
			//    }
			//},
          	{ headerName: "Tipo animal", field: "Tipoanimal", width: largura, checkboxSelection: true }
        	, { headerName: "Nome", field: "NomeAnimal", width: largura }
        	, { headerName: "Data Nascimento", field: "DataNascimento", width: largura }
    	];
	    return columnDefs;
	}
	
	App.animal.columnExport = function () {
		var columnEx = 'IFNULL(Tipoanimal, \'\') AS [Tipo animal], IFNULL(NomeAnimal, \'\') AS [Nome], IFNULL(DataNascimento, \'\') AS [Data Nascimento]';
	    return columnEx;
	}

	App.animal.Exportar = function () {
		Util.getItens(0, 0, $scope.animal.filtro, 
				function(listaRetorno){Util.ExportarExcel(listaRetorno,'animal', App.animal.columnExport())}, 
				function (erro) { serviceUtil.delegateErro(erro); })
	}

    //Documentação https://www.ag-grid.com/
    $scope.animal.gridOptions = Util.getDefaultGridOptions();
    $scope.animal.gridOptions.columnDefs = App.animal.columnDefinitions();

    Util.bindGrid($scope.animal);

    App.animal.filtroSelectedItem = function () {
		if ($scope.animal.gridOptions.api.getSelectedRows().length > 0) {
            return encodeURI('?key=' + $scope.animal.gridOptions.api.getSelectedRows()[0].IdAnimal);
        }
        else
            return null;
    }

    App.animal.AbrirEdicao = function () {
		if ($scope.animal.gridOptions.api.getSelectedRows().length == 0) {
            Util.Alerta('Selecione um item para edição.', 'warning');
            return;
        }

        if($scope.animal.gridOptions.api.getSelectedRows().length > 1){
            Util.Alerta('Selecione apenas um item para edição.', 'warning');
            return;
        }
		Util.AlertClear();
        //ngDialog.open({ closeByDocument: false, template: 'Animal.html', controller: 'AnimalController' });

    }
	
    App.animal.FecharEdicao = function () {
        //ngDialog.closeAll();
        $scope.Pesquisar();
    }

    App.animal.AbrirInclusao = function () {
	    $scope.animal.gridOptions.api.deselectAll();
		Util.AlertClear();
	    //ngDialog.open({ closeByDocument: false, template: 'Animal.html', controller: 'AnimalController' });
	}

    $scope.Pesquisar = function () {
         Util.bindGrid($scope.animal);
    }

    App.animal.Excluir = function (successo) {
		if ($scope.animal.gridOptions.api.getSelectedRows().length == 0) {
			Util.Alerta('Selecione algum item para exclusão.', 'warning');
			return;
		}
		
		Util.Confirm('Deseja realmente excluir este item?', 'Exclusão', function () {
			var animalLista = [];

			for (var i = 0; i < $scope.animal.gridOptions.api.getSelectedRows().length; i++) {
				animalLista.push({ "IdAnimal": $scope.animal.gridOptions.api.getSelectedRows()[i].IdAnimal });
			}

			Util.Excluir(animalLista, successo);
		});
	}

	$scope.Editar = App.animal.AbrirEdicao;
	$scope.Incluir = App.animal.AbrirInclusao;
	$scope.Exportar = App.animal.Exportar;
    
	$scope.Apagar = function () {
	    App.animal.Excluir(ValidarRetornoExclusao);
	}

	function ValidarRetornoExclusao(retorno) {
	    Util.Alerta(retorno.Mensagem, retorno.Sucesso);

	    if (retorno.Sucesso === true) {
    	    $scope.Pesquisar();
	    }
	}
});



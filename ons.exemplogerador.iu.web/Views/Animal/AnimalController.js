
//Controller Animal
App.controller("AnimalController", function ($scope, $rootScope, $http, Util) {
	$scope.animal = {};
	Util.getListasEdicao($scope, function (resultado) { $scope.animal.listasEdicao = resultado; ObterItem(); });

    function ObterItem()
	{
        var filtro = App.animal.filtroSelectedItem();

        if (filtro) {
			Util.getItem(App.animal.filtroSelectedItem(), function (item) { $scope.animal.itemEdicao = item; });
        }
        else
        {
            $scope.animal.itemEdicao = {};
        }
    }

    $scope.Salvar = function () {
        var filtro = App.animal.filtroSelectedItem();

		if (filtro) {
            Util.Atualizar(filtro, $scope.animal.itemEdicao, ValidarRetornoSalvar);
        }
        else {
        	$scope.animal.itemEdicao.IdAnimal = 0;

            Util.Inserir($scope.animal.itemEdicao, ValidarRetornoSalvar);
        }
    }

    function ValidarRetornoSalvar(retorno)
    {
        Util.Alerta(retorno.Mensagem, retorno.Sucesso);

        if (retorno.Sucesso === true) {
            App.animal.FecharEdicao();
        }
    }
});

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ons.exemplogerador.comum.ioc;
using ons.exemplogerador.comum;
using ons.exemplogerador.comum.Reflection;
using ons.exemplogerador.servico.ioc;
using ons.exemplogerador.negocio.contrato;
using ons.exemplogerador.entidade;
using ons.exemplogerador.servico.ioc.webapi;
using ons.exemplogerador.iu.web.Models;
using System.Web.Http;
using System.Data.Entity;
using ons.common.utilities.Helper;
using System.Net.Http;
using ons.common.providers;


namespace ons.exemplogerador.iu.web.Views
{

	/// <summary>
	/// Controller API para: Tabela com as informações dos animais
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	public partial class AnimalController : ons.exemplogerador.servico.ioc.webapi.BaseApiController
	{
        private IAnimalNegocio AnimalNegocio;
		private ons.common.providers.IHelperPOP PopHelper;
		private ITipoanimalNegocio TipoanimalNegocio;

        public AnimalController(ons.common.providers.IHelperPOP popHelper, IAnimalNegocio animalNegocio, ITipoanimalNegocio tipoanimalNegocio)
		{
			PopHelper = popHelper;
			AnimalNegocio = animalNegocio;
			TipoanimalNegocio = tipoanimalNegocio;
		}

        [POPAuthorize("Consultar Animal")]
        [HttpPost]
        public IEnumerable<dynamic> Get(AnimalFiltro Filtro, int skip = 0, int top = 0)
        {
            return AnimalNegocio.Query().Include(a=> a.Tipoanimal).Where(Filtro).Page(skip, top).Select(a=> new { a.IdAnimal, a.IdTipoanimal, a.NomeAnimal, a.DataNascimento, Tipoanimal = a.IdTipoanimal == null ? null : a.Tipoanimal.NomeTipoanimal }).ToList().Select(a=> new { a.IdAnimal, a.IdTipoanimal, a.NomeAnimal, DataNascimento = a.DataNascimento.HasValue ? a.DataNascimento.Value.ToShortDateString() + " " + a.DataNascimento.Value.ToLongTimeString() : null, a.Tipoanimal }).ToList();
        }

        [POPAuthorize("Consultar Animal")]
        [HttpPost]
        public int GetCount(AnimalFiltro Filtro)
        {
            return AnimalNegocio.Query().Where(Filtro).Count();
        }
		
        [POPCRUDAPIAuthorize()]
        public dynamic GetAutorizacoes()
        {
            return new
            {
                PodeLer = PopHelper.VerificarAcessoQualquerEscopo("Consultar Animal"),
                PodeEditar = PopHelper.VerificarAcessoQualquerEscopo("Editar Animal"),
                PodeIncluir = PopHelper.VerificarAcessoQualquerEscopo("Criar Animal"),
                PodeExcluir = PopHelper.VerificarAcessoQualquerEscopo("Excluir Animal"),
                PodeExportar = PopHelper.VerificarAcessoQualquerEscopo("Exportar Animal") 
            };
        }

		[POPCRUDAPIAuthorize()]
        public Animal GetItem(int key)
        {
		
            return AnimalNegocio.Query().Where(item => item.IdAnimal.Equals(key)).ToList().Select(a => new Animal() { IdAnimal = a.IdAnimal, IdTipoanimal = a.IdTipoanimal, NomeAnimal = a.NomeAnimal, DataNascimento = a.DataNascimento }).FirstOrDefault();
        }
		private Animal ObterItem(int key)
        {
            return AnimalNegocio.Query().Where(item => item.IdAnimal.Equals(key)).FirstOrDefault();
        }
		
		[POPCRUDAPIAuthorize()]
        public dynamic GetFiltroListas()
        {
            return new { TipoanimalLista = TipoanimalNegocio.Query().OrderBy(a=> a.NomeTipoanimal).Select(a => new { Valor = a.IdTipoanimal, Texto = a.NomeTipoanimal}).ToList() };
        }
		
		[POPCRUDAPIAuthorize()]
        public dynamic GetListasEdicao()
        {
            return new { TipoanimalLista = TipoanimalNegocio.Query().OrderBy(a=> a.NomeTipoanimal).Select(a => new { Valor = a.IdTipoanimal, Texto = a.NomeTipoanimal}).ToList() };
        }
		
		        // POST: odata/Animal
        [POPCRUDAPIAuthorize]
        public dynamic Post([FromBody]Animal animal)
        {
			AnimalNegocio.Criar(animal);
            AnimalNegocio.Salvar();
			return new { Sucesso = true, Mensagem = "Salvo com sucesso." };
        }

        // PATCH: odata/Animal
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public dynamic Patch(int key, [FromBody]Animal animal)
        {
			Animal original = ObterItem(key);
			ReflectionUtil.Fill(animal, original);
			
			AnimalNegocio.Atualizar(original);
            AnimalNegocio.Salvar();

			return new { Sucesso = true, Mensagem = "Atualizado com sucesso." };
        }

        // DELETE: odata/Animal
        [POPCRUDAPIAuthorize]
        [HttpPost]
        public dynamic Excluir(dynamic[] animalLista)
        {
            if (animalLista != null && animalLista.Length > 0)
            {
                foreach (var item in animalLista)
                {
                    Animal original = ObterItem((int)item.IdAnimal);
                    AnimalNegocio.Excluir(original);
                }
                AnimalNegocio.Salvar();

                return new { Sucesso = true, Mensagem = "Excluido com sucesso." };
            }
            else
            {
                return new { Sucesso = false, Mensagem = "Informe algum item para excluir." };
            }
        }
		
		protected override void Dispose(bool disposing)
        {
            AnimalNegocio = null;
			TipoanimalNegocio = null;
            base.Dispose(disposing);
        }

	}
}

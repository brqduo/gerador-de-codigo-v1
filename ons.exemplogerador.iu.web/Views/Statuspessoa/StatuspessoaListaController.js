
//Controller Statuspessoa
App.controller("StatuspessoaListaController", function ($scope, $rootScope, $http, Util) {
    //Inicialização.
    $scope.statuspessoa = {};
    $scope.statuspessoa.filtro = {};
    $scope.statuspessoa.filtroListas = {};
    App.statuspessoa = {};

    Util.getAutorizacoes(function (retorno) { $scope.permissao = retorno; })

    Util.getFiltroListas($scope, function (resultado) { $scope.statuspessoa.filtroListas = resultado; });

	App.statuspessoa.columnDefinitions = function () {
		var larguraTela = (getWidth() - 80);
		var largura = larguraTela * 1;
		
		if(largura < 150)
		{
			largura = 150;
		}
		
		var columnDefs = [
			// index da linha
			//{
			//    displayName: "#", width: 50, cellRenderer: function (params) {
			//        return params.node.id + 1;
			//    }
			//},
          	{ headerName: "Código", field: "CodigoStatuspessoa", width: largura, checkboxSelection: true }
    	];
	    return columnDefs;
	}
	
	App.statuspessoa.columnExport = function () {
		var columnEx = 'IFNULL(CodigoStatuspessoa, \'\') AS [Código]';
	    return columnEx;
	}

	App.statuspessoa.Exportar = function () {
		Util.getItens(0, 0, $scope.statuspessoa.filtro, 
				function(listaRetorno){Util.ExportarExcel(listaRetorno,'statuspessoa', App.statuspessoa.columnExport())}, 
				function (erro) { serviceUtil.delegateErro(erro); })
	}

    //Documentação https://www.ag-grid.com/
    $scope.statuspessoa.gridOptions = Util.getDefaultGridOptions();
    $scope.statuspessoa.gridOptions.columnDefs = App.statuspessoa.columnDefinitions();

    Util.bindGrid($scope.statuspessoa);

    App.statuspessoa.filtroSelectedItem = function () {
		if ($scope.statuspessoa.gridOptions.api.getSelectedRows().length > 0) {
            return encodeURI('?key=' + $scope.statuspessoa.gridOptions.api.getSelectedRows()[0].IdStatuspessoa);
        }
        else
            return null;
    }

    App.statuspessoa.AbrirEdicao = function () {
		if ($scope.statuspessoa.gridOptions.api.getSelectedRows().length == 0) {
            Util.Alerta('Selecione um item para edição.', 'warning');
            return;
        }

        if($scope.statuspessoa.gridOptions.api.getSelectedRows().length > 1){
            Util.Alerta('Selecione apenas um item para edição.', 'warning');
            return;
        }
		Util.AlertClear();
        //ngDialog.open({ closeByDocument: false, template: 'Statuspessoa.html', controller: 'StatuspessoaController' });

    }
	
    App.statuspessoa.FecharEdicao = function () {
        //ngDialog.closeAll();
        $scope.Pesquisar();
    }

    App.statuspessoa.AbrirInclusao = function () {
	    $scope.statuspessoa.gridOptions.api.deselectAll();
		Util.AlertClear();
	    //ngDialog.open({ closeByDocument: false, template: 'Statuspessoa.html', controller: 'StatuspessoaController' });
	}

    $scope.Pesquisar = function () {
         Util.bindGrid($scope.statuspessoa);
    }

    App.statuspessoa.Excluir = function (successo) {
		if ($scope.statuspessoa.gridOptions.api.getSelectedRows().length == 0) {
			Util.Alerta('Selecione algum item para exclusão.', 'warning');
			return;
		}
		
		Util.Confirm('Deseja realmente excluir este item?', 'Exclusão', function () {
			var statuspessoaLista = [];

			for (var i = 0; i < $scope.statuspessoa.gridOptions.api.getSelectedRows().length; i++) {
				statuspessoaLista.push({ "IdStatuspessoa": $scope.statuspessoa.gridOptions.api.getSelectedRows()[i].IdStatuspessoa });
			}

			Util.Excluir(statuspessoaLista, successo);
		});
	}

	$scope.Editar = App.statuspessoa.AbrirEdicao;
	$scope.Incluir = App.statuspessoa.AbrirInclusao;
	$scope.Exportar = App.statuspessoa.Exportar;
    
	$scope.Apagar = function () {
	    App.statuspessoa.Excluir(ValidarRetornoExclusao);
	}

	function ValidarRetornoExclusao(retorno) {
	    Util.Alerta(retorno.Mensagem, retorno.Sucesso);

	    if (retorno.Sucesso === true) {
    	    $scope.Pesquisar();
	    }
	}
});



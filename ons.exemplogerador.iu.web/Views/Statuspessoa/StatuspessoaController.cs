using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ons.exemplogerador.comum.ioc;
using ons.exemplogerador.comum;
using ons.exemplogerador.comum.Reflection;
using ons.exemplogerador.servico.ioc;
using ons.exemplogerador.negocio.contrato;
using ons.exemplogerador.entidade;
using ons.exemplogerador.servico.ioc.webapi;
using ons.exemplogerador.iu.web.Models;
using System.Web.Http;
using System.Data.Entity;
using ons.common.utilities.Helper;
using System.Net.Http;
using ons.common.providers;


namespace ons.exemplogerador.iu.web.Views
{

	/// <summary>
	/// Controller API para: Tabela com o status da pessoa. Ex.: Dono, Interessado em adotar ou Em processo de adoção
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:14
    /// </remarks>
	public partial class StatuspessoaController : ons.exemplogerador.servico.ioc.webapi.BaseApiController
	{
        private IStatuspessoaNegocio StatuspessoaNegocio;
		private ons.common.providers.IHelperPOP PopHelper;

        public StatuspessoaController(ons.common.providers.IHelperPOP popHelper, IStatuspessoaNegocio statuspessoaNegocio)
		{
			PopHelper = popHelper;
			StatuspessoaNegocio = statuspessoaNegocio;
		}

        [POPAuthorize("Consultar Statuspessoa")]
        [HttpPost]
        public IEnumerable<dynamic> Get(StatuspessoaFiltro Filtro, int skip = 0, int top = 0)
        {
            return StatuspessoaNegocio.Query().Where(Filtro).Page(skip, top).Select(a=> new { a.IdStatuspessoa, a.CodigoStatuspessoa }).ToList().Select(a=> new { a.IdStatuspessoa, a.CodigoStatuspessoa }).ToList();
        }

        [POPAuthorize("Consultar Statuspessoa")]
        [HttpPost]
        public int GetCount(StatuspessoaFiltro Filtro)
        {
            return StatuspessoaNegocio.Query().Where(Filtro).Count();
        }
		
        [POPCRUDAPIAuthorize()]
        public dynamic GetAutorizacoes()
        {
            return new
            {
                PodeLer = PopHelper.VerificarAcessoQualquerEscopo("Consultar Statuspessoa"),
                PodeEditar = PopHelper.VerificarAcessoQualquerEscopo("Editar Statuspessoa"),
                PodeIncluir = PopHelper.VerificarAcessoQualquerEscopo("Criar Statuspessoa"),
                PodeExcluir = PopHelper.VerificarAcessoQualquerEscopo("Excluir Statuspessoa"),
                PodeExportar = PopHelper.VerificarAcessoQualquerEscopo("Exportar Statuspessoa") 
            };
        }

		[POPCRUDAPIAuthorize()]
        public Statuspessoa GetItem(int key)
        {
		
            return StatuspessoaNegocio.Query().Where(item => item.IdStatuspessoa.Equals(key)).ToList().Select(a => new Statuspessoa() { IdStatuspessoa = a.IdStatuspessoa, CodigoStatuspessoa = a.CodigoStatuspessoa }).FirstOrDefault();
        }
		private Statuspessoa ObterItem(int key)
        {
            return StatuspessoaNegocio.Query().Where(item => item.IdStatuspessoa.Equals(key)).FirstOrDefault();
        }
		
		[POPCRUDAPIAuthorize()]
        public dynamic GetFiltroListas()
        {
            return new {  };
        }
		
		[POPCRUDAPIAuthorize()]
        public dynamic GetListasEdicao()
        {
            return new {  };
        }
		
		        // POST: odata/Statuspessoa
        [POPCRUDAPIAuthorize]
        public dynamic Post([FromBody]Statuspessoa statuspessoa)
        {
			StatuspessoaNegocio.Criar(statuspessoa);
            StatuspessoaNegocio.Salvar();
			return new { Sucesso = true, Mensagem = "Salvo com sucesso." };
        }

        // PATCH: odata/Statuspessoa
        [POPCRUDAPIAuthorize]
        [AcceptVerbs("PATCH", "MERGE")]
        public dynamic Patch(int key, [FromBody]Statuspessoa statuspessoa)
        {
			Statuspessoa original = ObterItem(key);
			ReflectionUtil.Fill(statuspessoa, original);
			
			StatuspessoaNegocio.Atualizar(original);
            StatuspessoaNegocio.Salvar();

			return new { Sucesso = true, Mensagem = "Atualizado com sucesso." };
        }

        // DELETE: odata/Statuspessoa
        [POPCRUDAPIAuthorize]
        [HttpPost]
        public dynamic Excluir(dynamic[] statuspessoaLista)
        {
            if (statuspessoaLista != null && statuspessoaLista.Length > 0)
            {
                foreach (var item in statuspessoaLista)
                {
                    Statuspessoa original = ObterItem((int)item.IdStatuspessoa);
                    StatuspessoaNegocio.Excluir(original);
                }
                StatuspessoaNegocio.Salvar();

                return new { Sucesso = true, Mensagem = "Excluido com sucesso." };
            }
            else
            {
                return new { Sucesso = false, Mensagem = "Informe algum item para excluir." };
            }
        }
		
		protected override void Dispose(bool disposing)
        {
            StatuspessoaNegocio = null;
            base.Dispose(disposing);
        }

	}
}

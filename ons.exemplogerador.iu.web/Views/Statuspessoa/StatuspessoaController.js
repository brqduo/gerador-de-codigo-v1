
//Controller Statuspessoa
App.controller("StatuspessoaController", function ($scope, $rootScope, $http, Util) {
	$scope.statuspessoa = {};
	Util.getListasEdicao($scope, function (resultado) { $scope.statuspessoa.listasEdicao = resultado; ObterItem(); });

    function ObterItem()
	{
        var filtro = App.statuspessoa.filtroSelectedItem();

        if (filtro) {
			Util.getItem(App.statuspessoa.filtroSelectedItem(), function (item) { $scope.statuspessoa.itemEdicao = item; });
        }
        else
        {
            $scope.statuspessoa.itemEdicao = {};
        }
    }

    $scope.Salvar = function () {
        var filtro = App.statuspessoa.filtroSelectedItem();

		if (filtro) {
            Util.Atualizar(filtro, $scope.statuspessoa.itemEdicao, ValidarRetornoSalvar);
        }
        else {
        	$scope.statuspessoa.itemEdicao.IdStatuspessoa = 0;

            Util.Inserir($scope.statuspessoa.itemEdicao, ValidarRetornoSalvar);
        }
    }

    function ValidarRetornoSalvar(retorno)
    {
        Util.Alerta(retorno.Mensagem, retorno.Sucesso);

        if (retorno.Sucesso === true) {
            App.statuspessoa.FecharEdicao();
        }
    }
});

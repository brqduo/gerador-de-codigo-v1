using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.exemplogerador.entidade;

namespace ons.exemplogerador.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para Tipoanimal
	/// Tabela de tipo de Animal. Ex.: Cachorro, Gato, Tartaruga, Cobra, etc.
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:14
    /// </remarks>
	public partial class TipoanimalMap : EntityTypeConfiguration<Tipoanimal>
	{

		public TipoanimalMap()
		{
		
			this.ToTable("TB_TPANIMAL");

			this.HasKey(t => t.IdTipoanimal);

			this.Property(t => t.IdTipoanimal).HasColumnName("ID_TPANIMAL").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);//.HasColumnType("int");//int//0//int
			this.Property(t => t.NomeTipoanimal).HasColumnName("NOM_TIPOANIMAL").IsRequired().HasMaxLength(20);//.HasColumnType("varchar");//varchar(20)//20//string
	
		}

	}
}

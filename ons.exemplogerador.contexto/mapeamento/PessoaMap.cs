using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.exemplogerador.entidade;

namespace ons.exemplogerador.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para Pessoa
	/// Tabela com as informações das pessoas donas de animais
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	public partial class PessoaMap : EntityTypeConfiguration<Pessoa>
	{

		public PessoaMap()
		{
		
			this.ToTable("TB_PESSOA");

			this.HasKey(t => t.IdPessoa);

			this.Property(t => t.IdPessoa).HasColumnName("ID_PESSOA").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);//.HasColumnType("int");//int//0//int
			this.Property(t => t.IdStatuspessoa).HasColumnName("ID_STATUSPESSOA").IsRequired();//.HasColumnType("int");//int//0//int?
			this.Property(t => t.NomePessoa).HasColumnName("NOM_PESSOA").IsRequired().HasMaxLength(100);//.HasColumnType("varchar");//varchar(100)//100//string
			this.Property(t => t.MailPessoa).HasColumnName("MAIL_PESSOA").IsRequired().HasMaxLength(100);//.HasColumnType("varchar");//varchar(100)//100//string
			this.Property(t => t.DataNascimento).HasColumnName("DAT_NASCIMENTO").IsRequired();//.HasColumnType("datetime");//datetime//0//DateTime?
			this.Property(t => t.CodigoFormatratamento).HasColumnName("COD_FORMATRATAMENTO").IsRequired().HasMaxLength(5);//.HasColumnType("varchar");//varchar(5)//5//string
			this.Property(t => t.NumeroCpf).HasColumnName("NUM_CPF").IsOptional().HasMaxLength(11);//.HasColumnType("char");//char(11)//11//string
			this.HasRequired(t => t.Statuspessoa).WithMany(t => t.PessoaLista).HasForeignKey(d => d.IdStatuspessoa);
	
		}

	}
}

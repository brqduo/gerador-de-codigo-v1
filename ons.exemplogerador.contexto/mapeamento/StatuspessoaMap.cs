using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.exemplogerador.entidade;

namespace ons.exemplogerador.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para Statuspessoa
	/// Tabela com o status da pessoa. Ex.: Dono, Interessado em adotar ou Em processo de adoção
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:14
    /// </remarks>
	public partial class StatuspessoaMap : EntityTypeConfiguration<Statuspessoa>
	{

		public StatuspessoaMap()
		{
		
			this.ToTable("TB_STATUSPESSOA");

			this.HasKey(t => t.IdStatuspessoa);

			this.Property(t => t.IdStatuspessoa).HasColumnName("ID_STATUSPESSOA").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);//.HasColumnType("int");//int//0//int
			this.Property(t => t.CodigoStatuspessoa).HasColumnName("COD_STATUSPESSOA").IsRequired().HasMaxLength(35);//.HasColumnType("varchar");//varchar(35)//35//string
	
		}

	}
}

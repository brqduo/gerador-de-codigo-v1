using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.exemplogerador.entidade;

namespace ons.exemplogerador.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para Animal
	/// Tabela com as informações dos animais
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	public partial class AnimalMap : EntityTypeConfiguration<Animal>
	{

		public AnimalMap()
		{
		
			this.ToTable("TB_ANIMAL");

			this.HasKey(t => t.IdAnimal);

			this.Property(t => t.IdAnimal).HasColumnName("ID_ANIMAL").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);//.HasColumnType("int");//int//0//int
			this.Property(t => t.IdTipoanimal).HasColumnName("ID_TPANIMAL").IsRequired();//.HasColumnType("int");//int//0//int?
			this.Property(t => t.NomeAnimal).HasColumnName("NOM_ANIMAL").IsRequired().HasMaxLength(70);//.HasColumnType("varchar");//varchar(70)//70//string
			this.Property(t => t.DataNascimento).HasColumnName("DAT_NASCIMENTO").IsRequired();//.HasColumnType("datetime");//datetime//0//DateTime?
			this.HasRequired(t => t.Tipoanimal).WithMany(t => t.AnimalLista).HasForeignKey(d => d.IdTipoanimal);
	
		}

	}
}

using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using ons.exemplogerador.entidade;

namespace ons.exemplogerador.contexto.mapeamento
{

	/// <summary>
	/// Data Mapping para Pessoaanimal
	/// Tabela Associativa Pessoa x Animal
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	public partial class PessoaanimalMap : EntityTypeConfiguration<Pessoaanimal>
	{

		public PessoaanimalMap()
		{
		
			this.ToTable("TB_PESSOAANIMAL");

			this.HasKey(t => t.IdPessoaanimal);

			this.Property(t => t.IdPessoaanimal).HasColumnName("ID_PESSOAANIMAL").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);//.HasColumnType("int");//int//0//int
			this.Property(t => t.IdPessoa).HasColumnName("ID_PESSOA").IsRequired();//.HasColumnType("int");//int//0//int?
			this.Property(t => t.IdAnimal).HasColumnName("ID_ANIMAL").IsRequired();//.HasColumnType("int");//int//0//int?
			this.HasRequired(t => t.Animal).WithMany(t => t.PessoaanimalLista).HasForeignKey(d => d.IdAnimal);
			this.HasRequired(t => t.Pessoa).WithMany(t => t.PessoaanimalLista).HasForeignKey(d => d.IdPessoa);
	
		}

	}
}

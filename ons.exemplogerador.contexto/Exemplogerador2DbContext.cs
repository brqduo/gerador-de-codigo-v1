using ons.common.auditoria;
using ons.common.auditoria.Models;
using ons.common.utilities.Xml;
using ons.exemplogerador.contexto.mapeamento;
using ons.exemplogerador.contexto.contrato;
using ons.exemplogerador.entidade;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.Linq;

namespace ons.exemplogerador.contexto
{

	/// <summary>
	/// DbContext de acesso a base ExemploGerador2
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:12
    /// </remarks>
	public partial class Exemplogerador2DbContext : DbContextAuditado, IExemplogerador2DbContext
	{
		
		public Exemplogerador2DbContext(ons.common.providers.IHelperPOP popHelper): base(popHelper, "Name=Exemplogerador2DbContext")
		{
            Database.SetInitializer<Exemplogerador2DbContext>(null);
            //this.Configuration.AutoDetectChangesEnabled = false;
            ((IObjectContextAdapter)this).ObjectContext.CommandTimeout = 120;
		}


		#region "OnModelCreating"
		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Configurations.Add(new AnimalMap());
			modelBuilder.Configurations.Add(new PessoaMap());
			modelBuilder.Configurations.Add(new PessoaanimalMap());
			modelBuilder.Configurations.Add(new StatuspessoaMap());
			modelBuilder.Configurations.Add(new TipoanimalMap());

		}
		#endregion


		public override void MontarMapa()
        {
            if (!mapaGerado)
            {
                lock (_lock)
                {
                    if (!mapaGerado)
                    {
                        base.MontarMapa();
                        var mapa = ons.common.auditoria.Models.Mapa.Instance;

						AnimalMap(mapa);
						PessoaMap(mapa);
						PessoaanimalMap(mapa);
						StatuspessoaMap(mapa);
						TipoanimalMap(mapa);


                        mapaGerado = true;
                    }
                }
            }
        }
		private void AnimalMap(Mapa mapa)
        {
            var classeAnimal = mapa.RecuperarClasse<Animal>();
            classeAnimal.AdicionarCaminhoExibicao(c => c.NomeAnimal);
            classeAnimal.NomeExterno = @"Animal";

            var propIdAnimal = classeAnimal.RecuperarPropriedade(c => c.IdAnimal);
            propIdAnimal.NomeExterno = @"Chave";

            var propIdTipoanimal = classeAnimal.RecuperarPropriedade(c => c.IdTipoanimal);
            propIdTipoanimal.NomeExterno = @"Chave Tipo";

            var propNomeAnimal = classeAnimal.RecuperarPropriedade(c => c.NomeAnimal);
            propNomeAnimal.NomeExterno = @"Nome";

            var propDataNascimento = classeAnimal.RecuperarPropriedade(c => c.DataNascimento);
            propDataNascimento.NomeExterno = @"Data Nascimento";



        }	

		private void PessoaMap(Mapa mapa)
        {
            var classePessoa = mapa.RecuperarClasse<Pessoa>();
            classePessoa.AdicionarCaminhoExibicao(c => c.NomePessoa);
            classePessoa.NomeExterno = @"Pessoa";

            var propIdPessoa = classePessoa.RecuperarPropriedade(c => c.IdPessoa);
            propIdPessoa.NomeExterno = @"Chave";

            var propIdStatuspessoa = classePessoa.RecuperarPropriedade(c => c.IdStatuspessoa);
            propIdStatuspessoa.NomeExterno = @"Chave Status";

            var propNomePessoa = classePessoa.RecuperarPropriedade(c => c.NomePessoa);
            propNomePessoa.NomeExterno = @"Nome";

            var propMailPessoa = classePessoa.RecuperarPropriedade(c => c.MailPessoa);
            propMailPessoa.NomeExterno = @"E-mail";

            var propDataNascimento = classePessoa.RecuperarPropriedade(c => c.DataNascimento);
            propDataNascimento.NomeExterno = @"Data Nascimento";

            var propCodigoFormatratamento = classePessoa.RecuperarPropriedade(c => c.CodigoFormatratamento);
            propCodigoFormatratamento.NomeExterno = @"Código Formatratamento";

            var propNumeroCpf = classePessoa.RecuperarPropriedade(c => c.NumeroCpf);
            propNumeroCpf.NomeExterno = @"Número Cpf";



        }	

		private void PessoaanimalMap(Mapa mapa)
        {
            var classePessoaanimal = mapa.RecuperarClasse<Pessoaanimal>();
            classePessoaanimal.AdicionarCaminhoExibicao(c => c.IdPessoa);
            classePessoaanimal.NomeExterno = @"Pessoaanimal";

            var propIdPessoaanimal = classePessoaanimal.RecuperarPropriedade(c => c.IdPessoaanimal);
            propIdPessoaanimal.NomeExterno = @"Chave";

            var propIdPessoa = classePessoaanimal.RecuperarPropriedade(c => c.IdPessoa);
            propIdPessoa.NomeExterno = @"Chave Pessoa";

            var propIdAnimal = classePessoaanimal.RecuperarPropriedade(c => c.IdAnimal);
            propIdAnimal.NomeExterno = @"Chave Animal";



        }	

		private void StatuspessoaMap(Mapa mapa)
        {
            var classeStatuspessoa = mapa.RecuperarClasse<Statuspessoa>();
            classeStatuspessoa.AdicionarCaminhoExibicao(c => c.CodigoStatuspessoa);
            classeStatuspessoa.NomeExterno = @"Status pessoa";

            var propIdStatuspessoa = classeStatuspessoa.RecuperarPropriedade(c => c.IdStatuspessoa);
            propIdStatuspessoa.NomeExterno = @"Chave";

            var propCodigoStatuspessoa = classeStatuspessoa.RecuperarPropriedade(c => c.CodigoStatuspessoa);
            propCodigoStatuspessoa.NomeExterno = @"Código";



        }	

		private void TipoanimalMap(Mapa mapa)
        {
            var classeTipoanimal = mapa.RecuperarClasse<Tipoanimal>();
            classeTipoanimal.AdicionarCaminhoExibicao(c => c.NomeTipoanimal);
            classeTipoanimal.NomeExterno = @"Tipo animal";

            var propIdTipoanimal = classeTipoanimal.RecuperarPropriedade(c => c.IdTipoanimal);
            propIdTipoanimal.NomeExterno = @"Chave Tipo animal";

            var propNomeTipoanimal = classeTipoanimal.RecuperarPropriedade(c => c.NomeTipoanimal);
            propNomeTipoanimal.NomeExterno = @"Nome";



        }	



        protected override void Dispose(bool disposing)
        {
			base.Dispose(disposing);
        }
		
		public void Dispose()
        {
			Dispose(false);
		}
	}
}

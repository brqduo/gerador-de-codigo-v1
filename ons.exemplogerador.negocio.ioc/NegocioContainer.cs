using Castle.DynamicProxy;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ons.exemplogerador.negocio.ioc;
using ons.exemplogerador.negocio.contrato;
using ons.exemplogerador.negocio;
namespace ons.exemplogerador.negocio.ioc
{

	/// <summary>
	/// Mapeameto da inversão de controle do negocio
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	public class NegocioContainer
	{
        private static readonly ILog Log = LogManager.GetLogger(typeof(NegocioContainer));
		
        #region IInterceptor Members
		
		/// <summary>
        /// Registra o mapa de inversão de controle do negocio e suas dependencias.
        /// </summary>
        /// <param name="container">Container principal</param>
        public static void RegisterConfigure(IWindsorContainer container)
        {
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            //
            try
            {
				//Exemplo de customização de mapeamento
				//if (ConfigurationManager.AppSettings["UsaProxy"] != null && ConfigurationManager.AppSettings["UsaProxy"] == "1")
                //{
                //    container.Register(Component.For<INegocionegocio.iocCustom>().Interceptors<Inegocio.iocInterceptor>().ImplementedBy<Negocionegocio.iocCustomProxy>().LifestylePerWebRequest());
                //}
                //else
                //{
                //    container.Register(Component.For<I<Negocionegocio.iocCustom>().Interceptors<Inegocio.iocInterceptor>().ImplementedBy<Negocionegocio.iocEntity>().LifestylePerWebRequest());
                //}
				contexto.ioc.DbContextContainer.RegisterConfigure(container);

				Log.Debug("NegocioContainer|RegisterConfigure|Registrando mapa de classes do negocio");
				//Interceptador
                container.Register(Component.For<INegocioInterceptor>().ImplementedBy<NegocioInterceptor>().LifeStyle.Transient);

				//Classes
				container.Register(Component.For<IAnimalNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<AnimalNegocio>().LifestylePerWebRequest());
				container.Register(Component.For<IPessoaNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<PessoaNegocio>().LifestylePerWebRequest());
				container.Register(Component.For<IPessoaanimalNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<PessoaanimalNegocio>().LifestylePerWebRequest());
				container.Register(Component.For<IStatuspessoaNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<StatuspessoaNegocio>().LifestylePerWebRequest());
				container.Register(Component.For<ITipoanimalNegocio>().Interceptors<INegocioInterceptor>().ImplementedBy<TipoanimalNegocio>().LifestylePerWebRequest());

                
            }
            finally
            {
                stopwatch.Stop();
                Log.DebugFormat("NegocioContainer|RegisterConfigure|Terminou o register do mapa de classes do negocio.|{0}", stopwatch.ElapsedMilliseconds.ToString());
            }
        }

        #endregion

	}
}

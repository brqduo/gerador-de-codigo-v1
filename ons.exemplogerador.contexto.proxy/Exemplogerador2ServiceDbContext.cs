using ons.exemplogerador.contexto.contrato;
using log4net;
using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace ons.exemplogerador.contexto.proxy
{

	/// <summary>
	/// DbContext de acesso a base ExemploGerador2
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	public partial class Exemplogerador2ServiceDbContext : ServiceDbContext, IExemplogerador2DbContext
	{

		public Exemplogerador2ServiceDbContext(ons.common.providers.IHelperPOP popHelper): base(popHelper)
		{
		}

        //protected override DataServiceContext Container
        //{
        //    get
        //    {
        //        if (container == null)
        //        {
        //            var uri = new Uri(PopHelper.ObterParametro("EXEMPLOGERADORServiceUri"));
        //            
        //            container = new DataServiceContext(uri, System.Data.Services.Common.DataServiceProtocolVersion.V3);
        //            container.Credentials = new NetworkCredential(PopHelper.ObterParametro("EXEMPLOGERADORServiceUserName"), PopHelper.ObterParametro("EXEMPLOGERADORServicePassword", true));
        //            container.SendingRequest2 += Container_SendingRequestTicket;
        //            container.IgnoreMissingProperties = true;
        //        }
        //        return container;
        //    }
        //}
		
        public override string urlServico
        {
            get
			{
				return PopHelper.ObterParametro("EXEMPLOGERADORServiceUri");
			}
        }

        public override string LoginServico
        {
            get
			{
				return PopHelper.ObterParametro("EXEMPLOGERADORServiceUserName");
			}
        }

        public override string SenhaServico
        {
            get
			{
				return PopHelper.ObterParametro("EXEMPLOGERADORServicePassword", true);
			}
        }
		
		public override void Dispose()
        {
            container = null;
			base.Dispose();
		}
	}
}

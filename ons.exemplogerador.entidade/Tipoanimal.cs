using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.exemplogerador.entidade
{

	/// <summary>
	/// Tabela de tipo de Animal. Ex.: Cachorro, Gato, Tartaruga, Cobra, etc.
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:14
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "Tipoanimal", Namespace = "http://schemas.ons.org.br/2013/04/Tipoanimal")]
	[DataServiceKey("IdTipoanimal")]
	public partial class Tipoanimal
	{
	
		public Tipoanimal()
		{
			AnimalLista = new List<Animal>();
		}


		#region "Propriedades"

		/// <summary>
		///	Identificador do Tipo de Animal
		///</summary>
		[DataMember(Name = "IdTipoanimal")]
		public virtual int IdTipoanimal { get; set; }
		/// <summary>
		///	Nome do Tipo do Animal. Ex: Cachorro, Gato, Cobra, Réptil, etc.
		///</summary>
		[DataMember(Name = "NomeTipoanimal")]
		public virtual string NomeTipoanimal { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	Tabela com as informações dos animais
		///</summary>
		[DataMember(Name = "AnimalLista")]
		public virtual IList<Animal> AnimalLista { get; set; }
	

		#endregion

	}
}

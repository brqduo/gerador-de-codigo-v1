﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ons.exemplogerador.entidade
{
    [Serializable()]
    [DataContract(Name = "Animal", Namespace = "http://schemas.ons.org.br/2013/04/RelAnimal")]
    public class RelAnimal
    {
		[DataMember(Name = "IdTipoanimal")]
        public int? IdTipoanimal { get; set; }

		[DataMember(Name = "NomeTipoanimal")]
        public string NomeTipoanimal { get; set; }


		[DataMember(Name = "Quantidade")]
        public int Quantidade { get; set; }
    }
}

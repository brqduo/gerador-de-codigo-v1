using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.exemplogerador.entidade
{

	/// <summary>
	/// Tabela Associativa Pessoa x Animal
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "Pessoaanimal", Namespace = "http://schemas.ons.org.br/2013/04/Pessoaanimal")]
	[DataServiceKey("IdPessoaanimal")]
	public partial class Pessoaanimal
	{
	
		public Pessoaanimal()
		{
		}


		#region "Propriedades"

		/// <summary>
		///	
		///</summary>
		[DataMember(Name = "IdPessoaanimal")]
		public virtual int IdPessoaanimal { get; set; }
		/// <summary>
		///	Identificador da tabela de pessoas
		///</summary>
		[DataMember(Name = "IdPessoa")]
		public virtual int? IdPessoa { get; set; }
		/// <summary>
		///	Identificador da tabela de animal
		///</summary>
		[DataMember(Name = "IdAnimal")]
		public virtual int? IdAnimal { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	Tabela com as informações dos animais
		///</summary>
		[DataMember(Name = "Animal")]
		public virtual Animal Animal { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	Tabela com as informações das pessoas donas de animais
		///</summary>
		[DataMember(Name = "Pessoa")]
		public virtual Pessoa Pessoa { get; set; }
	

		#endregion

	}
}

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.exemplogerador.entidade
{

	/// <summary>
	/// Tabela com as informações dos animais
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "Animal", Namespace = "http://schemas.ons.org.br/2013/04/Animal")]
	[DataServiceKey("IdAnimal")]
	public partial class Animal
	{
	
		public Animal()
		{
			PessoaanimalLista = new List<Pessoaanimal>();
		}


		#region "Propriedades"

		/// <summary>
		///	Identificador da tabela de animal
		///</summary>
		[DataMember(Name = "IdAnimal")]
		public virtual int IdAnimal { get; set; }
		/// <summary>
		///	Identificador do Tipo de Animal
		///</summary>
		[DataMember(Name = "IdTipoanimal")]
		public virtual int? IdTipoanimal { get; set; }
		/// <summary>
		///	Nome do Animal
		///</summary>
		[DataMember(Name = "NomeAnimal")]
		public virtual string NomeAnimal { get; set; }
		/// <summary>
		///	Data de Nascimento
		///</summary>
		[DataMember(Name = "DataNascimento")]
		public virtual DateTime? DataNascimento { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	Tabela de tipo de Animal. Ex.: Cachorro, Gato, Tartaruga, Cobra, etc.
		///</summary>
		[DataMember(Name = "Tipoanimal")]
		public virtual Tipoanimal Tipoanimal { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	Tabela Associativa Pessoa x Animal
		///</summary>
		[DataMember(Name = "PessoaanimalLista")]
		public virtual IList<Pessoaanimal> PessoaanimalLista { get; set; }
	

		#endregion

	}
}

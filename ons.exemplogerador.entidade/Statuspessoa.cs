using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.exemplogerador.entidade
{

	/// <summary>
	/// Tabela com o status da pessoa. Ex.: Dono, Interessado em adotar ou Em processo de adoção
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:14
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "Statuspessoa", Namespace = "http://schemas.ons.org.br/2013/04/Statuspessoa")]
	[DataServiceKey("IdStatuspessoa")]
	public partial class Statuspessoa
	{
	
		public Statuspessoa()
		{
			PessoaLista = new List<Pessoa>();
		}


		#region "Propriedades"

		/// <summary>
		///	Identificador do Status da Pessoa
		///</summary>
		[DataMember(Name = "IdStatuspessoa")]
		public virtual int IdStatuspessoa { get; set; }
		/// <summary>
		///	Status da Pessoa
		///</summary>
		[DataMember(Name = "CodigoStatuspessoa")]
		public virtual string CodigoStatuspessoa { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	Tabela com as informações das pessoas donas de animais
		///</summary>
		[DataMember(Name = "PessoaLista")]
		public virtual IList<Pessoa> PessoaLista { get; set; }
	

		#endregion

	}
}

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Data.Services.Common;

namespace ons.exemplogerador.entidade
{

	/// <summary>
	/// Tabela com as informações das pessoas donas de animais
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	[Serializable()] 
	[DataContract(Name = "Pessoa", Namespace = "http://schemas.ons.org.br/2013/04/Pessoa")]
	[DataServiceKey("IdPessoa")]
	public partial class Pessoa
	{
	
		public Pessoa()
		{
			PessoaanimalLista = new List<Pessoaanimal>();
		}


		#region "Propriedades"

		/// <summary>
		///	Identificador da tabela de pessoas
		///</summary>
		[DataMember(Name = "IdPessoa")]
		public virtual int IdPessoa { get; set; }
		/// <summary>
		///	Identificador do Status da Pessoa
		///</summary>
		[DataMember(Name = "IdStatuspessoa")]
		public virtual int? IdStatuspessoa { get; set; }
		/// <summary>
		///	Nome da Pessoa
		///</summary>
		[DataMember(Name = "NomePessoa")]
		public virtual string NomePessoa { get; set; }
		/// <summary>
		///	Endereço de e-mail da pessoa
		///</summary>
		[DataMember(Name = "MailPessoa")]
		public virtual string MailPessoa { get; set; }
		/// <summary>
		///	Data de Nascimento da Pessoa
		///</summary>
		[DataMember(Name = "DataNascimento")]
		public virtual DateTime? DataNascimento { get; set; }
		/// <summary>
		///	Código Forma de Tratamento para as pessoas. Valores possíveis: Sr. Sra. Srta.
		///</summary>
		[DataMember(Name = "CodigoFormatratamento")]
		public virtual string CodigoFormatratamento { get; set; }
		/// <summary>
		///	Número do Cadastro de Pessoa FÍsica da Pessoa
		///</summary>
		[DataMember(Name = "NumeroCpf")]
		public virtual string NumeroCpf { get; set; }
		/// <summary>
		/// [relationNotAssTables]
		///	Tabela com o status da pessoa. Ex.: Dono, Interessado em adotar ou Em processo de adoção
		///</summary>
		[DataMember(Name = "Statuspessoa")]
		public virtual Statuspessoa Statuspessoa { get; set; }
		/// <summary>
		/// [relation1xNTables]
		///	Tabela Associativa Pessoa x Animal
		///</summary>
		[DataMember(Name = "PessoaanimalLista")]
		public virtual IList<Pessoaanimal> PessoaanimalLista { get; set; }
	

		#endregion

	}
}

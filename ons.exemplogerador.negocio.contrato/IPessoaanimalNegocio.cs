using ons.exemplogerador.entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ons.exemplogerador.negocio.contrato
{

	/// <summary>
	/// Repositorio para: Tabela Associativa Pessoa x Animal
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	public interface IPessoaanimalNegocio : INegocioBase<Pessoaanimal>
	{

		
		Pessoaanimal Carregar(int idPessoaanimal);
	}
}

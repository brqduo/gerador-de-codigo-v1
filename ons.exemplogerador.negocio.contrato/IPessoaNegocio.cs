using ons.exemplogerador.entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ons.exemplogerador.negocio.contrato
{

	/// <summary>
	/// Repositorio para: Tabela com as informações das pessoas donas de animais
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	public interface IPessoaNegocio : INegocioBase<Pessoa>
	{

		
		Pessoa Carregar(int idPessoa);
	}
}

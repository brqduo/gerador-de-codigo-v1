using ons.exemplogerador.comum.ioc;
using ons.exemplogerador.entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ons.exemplogerador.negocio.contrato
{

    /// <summary>
    /// Repositorio para: Tabela de tipo de Animal. Ex.: Cachorro, Gato, Tartaruga, Cobra, etc.
    /// </summary> 
    /// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:14
    /// </remarks>
    public interface ITipoanimalNegocio : INegocioBase<Tipoanimal>
    {


        Tipoanimal Carregar(int idTipoanimal);
        [RunatServer(Route = "Relatorio/ObterQuantidadeAnimalPorTipo")]
        List<RelAnimal> ObterQuantidadeAnimalPorTipo();

    }
}

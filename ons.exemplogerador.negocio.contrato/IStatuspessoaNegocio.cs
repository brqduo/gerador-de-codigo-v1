using ons.exemplogerador.entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ons.exemplogerador.negocio.contrato
{

	/// <summary>
	/// Repositorio para: Tabela com o status da pessoa. Ex.: Dono, Interessado em adotar ou Em processo de adoção
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:14
    /// </remarks>
	public interface IStatuspessoaNegocio : INegocioBase<Statuspessoa>
	{

		
		Statuspessoa Carregar(int idStatuspessoa);
	}
}

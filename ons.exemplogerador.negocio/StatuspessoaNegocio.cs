using ons.exemplogerador.entidade;
using ons.exemplogerador.comum;
using ons.exemplogerador.negocio.contrato;
using ons.exemplogerador.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.exemplogerador.negocio
{

	/// <summary>
	/// Camada de negócio para: Tabela com o status da pessoa. Ex.: Dono, Interessado em adotar ou Em processo de adoção
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:14
    /// </remarks>
	public partial class StatuspessoaNegocio : NegocioBase<Statuspessoa>, IStatuspessoaNegocio
	{
        public StatuspessoaNegocio(IExemplogerador2DbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public Statuspessoa Carregar(int idStatuspessoa)
        {
           var retorno = Query().Where(item => item.IdStatuspessoa.Equals(idStatuspessoa)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<Statuspessoa> AplicarOrdem(IQueryable<Statuspessoa> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<Statuspessoa> retorno = query;

			retorno = query.OrderBy(b => b.CodigoStatuspessoa);

            return retorno;
        }
		
		public override IQueryable<Statuspessoa> AplicarAutorizacao(IQueryable<Statuspessoa> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<Statuspessoa> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar Statuspessoa" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(Statuspessoa entity)
        {
			if(entity == null)
                throw new BusinessException(@"Status pessoa deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.CodigoStatuspessoa.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Código deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.CodigoStatuspessoa != null && entity.CodigoStatuspessoa.Length > 35)
                throw new BusinessException(@"O campo Código deve ter tamanho máximo de 35 caracteres.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(Statuspessoa entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(Statuspessoa entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(Statuspessoa entity)
        {
			if(entity == null)
                throw new BusinessException(@"Status pessoa deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.PessoaLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Pessoa.", common.schemas.MessageType.ERROR);

	
        }
	}
}

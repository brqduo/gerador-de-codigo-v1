using ons.exemplogerador.entidade;
using ons.exemplogerador.comum;
using ons.exemplogerador.negocio.contrato;
using ons.exemplogerador.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.exemplogerador.negocio
{

	/// <summary>
	/// Camada de negócio para: Tabela com as informações das pessoas donas de animais
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	public partial class PessoaNegocio : NegocioBase<Pessoa>, IPessoaNegocio
	{
        public PessoaNegocio(IExemplogerador2DbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public Pessoa Carregar(int idPessoa)
        {
           var retorno = Query().Where(item => item.IdPessoa.Equals(idPessoa)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<Pessoa> AplicarOrdem(IQueryable<Pessoa> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<Pessoa> retorno = query;

			retorno = query.OrderBy(b => b.NomePessoa);

            return retorno;
        }
		
		public override IQueryable<Pessoa> AplicarAutorizacao(IQueryable<Pessoa> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<Pessoa> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar Pessoa" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(Pessoa entity)
        {
			if(entity == null)
                throw new BusinessException(@"Pessoa deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.IdStatuspessoa == null)
                throw new BusinessException(@"O campo Chave Status deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.NomePessoa.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Nome deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.NomePessoa != null && entity.NomePessoa.Length > 100)
                throw new BusinessException(@"O campo Nome deve ter tamanho máximo de 100 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.MailPessoa.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo E-mail deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.MailPessoa != null && entity.MailPessoa.Length > 100)
                throw new BusinessException(@"O campo E-mail deve ter tamanho máximo de 100 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.DataNascimento == null)
                throw new BusinessException(@"O campo Data Nascimento deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.CodigoFormatratamento.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Código Formatratamento deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.CodigoFormatratamento != null && entity.CodigoFormatratamento.Length > 5)
                throw new BusinessException(@"O campo Código Formatratamento deve ter tamanho máximo de 5 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.NumeroCpf != null && entity.NumeroCpf.Length > 11)
                throw new BusinessException(@"O campo Número Cpf deve ter tamanho máximo de 11 caracteres.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(Pessoa entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(Pessoa entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(Pessoa entity)
        {
			if(entity == null)
                throw new BusinessException(@"Pessoa deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.PessoaanimalLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Pessoaanimal.", common.schemas.MessageType.ERROR);

	
        }
	}
}

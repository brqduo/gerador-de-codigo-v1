using ons.exemplogerador.entidade;
using ons.exemplogerador.comum;
using ons.exemplogerador.negocio.contrato;
using ons.exemplogerador.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.exemplogerador.negocio
{

	/// <summary>
	/// Camada de negócio para: Tabela com as informações dos animais
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	public partial class AnimalNegocio : NegocioBase<Animal>, IAnimalNegocio
	{
        public AnimalNegocio(IExemplogerador2DbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public Animal Carregar(int idAnimal)
        {
           var retorno = Query().Where(item => item.IdAnimal.Equals(idAnimal)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<Animal> AplicarOrdem(IQueryable<Animal> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<Animal> retorno = query;

			retorno = query.OrderBy(b => b.NomeAnimal);

            return retorno;
        }
		
		public override IQueryable<Animal> AplicarAutorizacao(IQueryable<Animal> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<Animal> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar Animal" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(Animal entity)
        {
			if(entity == null)
                throw new BusinessException(@"Animal deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.IdTipoanimal == null)
                throw new BusinessException(@"O campo Chave Tipo deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.NomeAnimal.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Nome deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.NomeAnimal != null && entity.NomeAnimal.Length > 70)
                throw new BusinessException(@"O campo Nome deve ter tamanho máximo de 70 caracteres.", common.schemas.MessageType.ERROR);

            if (entity.DataNascimento == null)
                throw new BusinessException(@"O campo Data Nascimento deve ser informado.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(Animal entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(Animal entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(Animal entity)
        {
			if(entity == null)
                throw new BusinessException(@"Animal deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.PessoaanimalLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Pessoaanimal.", common.schemas.MessageType.ERROR);

	
        }
	}
}

using ons.exemplogerador.entidade;
using ons.exemplogerador.comum;
using ons.exemplogerador.negocio.contrato;
using ons.exemplogerador.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.exemplogerador.negocio
{

	/// <summary>
	/// Camada de negócio para: Tabela Associativa Pessoa x Animal
	/// </summary> 
	/// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:13
    /// </remarks>
	public partial class PessoaanimalNegocio : NegocioBase<Pessoaanimal>, IPessoaanimalNegocio
	{
        public PessoaanimalNegocio(IExemplogerador2DbContext context, ons.common.providers.IHelperPOP popHelper) : base(context, popHelper)
		{
			
		}
		
		
		
		public Pessoaanimal Carregar(int idPessoaanimal)
        {
           var retorno = Query().Where(item => item.IdPessoaanimal.Equals(idPessoaanimal)).FirstOrDefault();
           return retorno;
        }
		
		public override IQueryable<Pessoaanimal> AplicarOrdem(IQueryable<Pessoaanimal> query)
        {
			//TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<Pessoaanimal> retorno = query;

			retorno = query.OrderBy(b => b.IdPessoa);

            return retorno;
        }
		
		public override IQueryable<Pessoaanimal> AplicarAutorizacao(IQueryable<Pessoaanimal> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<Pessoaanimal> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar Pessoaanimal" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;        
		}
		private void ValidarObrigatoriedade(Pessoaanimal entity)
        {
			if(entity == null)
                throw new BusinessException(@"Pessoaanimal deve ser informado.", common.schemas.MessageType.ERROR);
				
            if (entity.IdPessoa == null)
                throw new BusinessException(@"O campo Chave Pessoa deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.IdAnimal == null)
                throw new BusinessException(@"O campo Chave Animal deve ser informado.", common.schemas.MessageType.ERROR);


        }

		public override void ValidarCriar(Pessoaanimal entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarAtualizar(Pessoaanimal entity)
        {
			ValidarObrigatoriedade(entity);
        }
		
        public override void ValidarExcluir(Pessoaanimal entity)
        {
			if(entity == null)
                throw new BusinessException(@"Pessoaanimal deve ser informado.", common.schemas.MessageType.ERROR);

	
        }
	}
}

using ons.exemplogerador.entidade;
using ons.exemplogerador.comum;
using ons.exemplogerador.negocio.contrato;
using ons.exemplogerador.contexto.contrato;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ons.common.utilities.Helper;
using ons.common.utilities.ExceptionHandling;

namespace ons.exemplogerador.negocio
{

    /// <summary>
    /// Camada de negócio para: Tabela de tipo de Animal. Ex.: Cachorro, Gato, Tartaruga, Cobra, etc.
    /// </summary> 
    /// <remarks>
    /// White Brasil - Code Generation: 2018/05/10 16:57:14
    /// </remarks>
    public partial class TipoanimalNegocio : NegocioBase<Tipoanimal>, ITipoanimalNegocio
    {
        IAnimalNegocio AnimalNegocio;
        public TipoanimalNegocio(IExemplogerador2DbContext context, ons.common.providers.IHelperPOP popHelper, IAnimalNegocio animalNegocio) : base(context, popHelper)
        {
            AnimalNegocio = animalNegocio;
        }



        public Tipoanimal Carregar(int idTipoanimal)
        {
            var retorno = Query().Where(item => item.IdTipoanimal.Equals(idTipoanimal)).FirstOrDefault();
            return retorno;
        }

        public override IQueryable<Tipoanimal> AplicarOrdem(IQueryable<Tipoanimal> query)
        {
            //TODO: [Code Generation] = Adicionar ordenação de acesso ao dado.
            IQueryable<Tipoanimal> retorno = query;

            retorno = query.OrderBy(b => b.NomeTipoanimal);

            return retorno;
        }

        public override IQueryable<Tipoanimal> AplicarAutorizacao(IQueryable<Tipoanimal> query)
        {
            //TODO: [Code Generation] = Adicionar validação de escopo de acesso ao dado.
            IQueryable<Tipoanimal> retorno = query;

            //Exemplo de uso:
            //Tipos de escopo: "ONS ou CONC ou AGENTES ou INST ou CENTROS ou CONTATO ou APLICACAO ou TANQUEMATUTENCAO"
            var escopos = PopHelper.ObterAcessosPorTicket(PopHelper.TicketUsuario, new string[] { "Consultar Tipoanimal" });//Informar a operação e o tipo de escopo.

            if (escopos.Count(e => e.CodTpEscopo == "ONS") == 0)
            {
                //var ids = escopos.Where(a => a.CodTpEscopo == "[Tipo de Escopo]").Select(a => a.IdEscopo);
                //retorno = retorno.WhereIn(a => a.[filtro], ids); //Filtrar pelo escopo especifico.
            }

            return retorno;
        }
        private void ValidarObrigatoriedade(Tipoanimal entity)
        {
            if (entity == null)
                throw new BusinessException(@"Tipo animal deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.NomeTipoanimal.IsNullOrEmptyOrSpace())
                throw new BusinessException(@"O campo Nome deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.NomeTipoanimal != null && entity.NomeTipoanimal.Length > 20)
                throw new BusinessException(@"O campo Nome deve ter tamanho máximo de 20 caracteres.", common.schemas.MessageType.ERROR);


        }

        public override void ValidarCriar(Tipoanimal entity)
        {
            ValidarObrigatoriedade(entity);
        }

        public override void ValidarAtualizar(Tipoanimal entity)
        {
            ValidarObrigatoriedade(entity);
        }

        public override void ValidarExcluir(Tipoanimal entity)
        {
            if (entity == null)
                throw new BusinessException(@"Tipo animal deve ser informado.", common.schemas.MessageType.ERROR);

            if (entity.AnimalLista.Count > 0)//relation1xNTables
                throw new BusinessException(@"A exclusão não pode ser realizada devido ao relacionamento com Animal.", common.schemas.MessageType.ERROR);


        }



        public List<RelAnimal> ObterQuantidadeAnimalPorTipo()
        {
            //var consulta = from a in AnimalNegocio.Query()
            //               group a by a.Tipoanimal into g
            //               orderby g.Key.NomeTipoanimal
            //               select new RelAnimal() { NomeTipoanimal = g.Key.NomeTipoanimal, IdTipoanimal = g.Key.IdTipoanimal, Quantidade = g.Count() };

            //return consulta.ToList();


            var consulta = (from a in this.Query().Include(a=> a.AnimalLista)
                            orderby a.NomeTipoanimal
                            select new { NomeTipoanimal = a.NomeTipoanimal, IdTipoanimal = a.IdTipoanimal, AnimalLista = a.AnimalLista }).ToList();


            var retorno = consulta.Select(a => new RelAnimal() { NomeTipoanimal = a.NomeTipoanimal, IdTipoanimal = a.IdTipoanimal, Quantidade = a.AnimalLista.Count() });

            return retorno.ToList();
        }
    }
}
